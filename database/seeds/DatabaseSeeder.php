<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
        // $this->call(UsersTableSeeder::class);
        DB::table('admins')->insert([
            'username' => 'mai',
            'email' => 'mai.@gmail.com',
            'password' => bcrypt("mai123"),
        ]);

        DB::table('users')->insert([
            'username' => 'rehab',
            'email' => 'rehab.@gmail.com',
            'password' => bcrypt("rehab123"),
        ]);

        DB::table('levels')->insert([
            'name' => 'level1',
            'score' => 20

        ]);
        DB::table('levels')->insert([
            'name' => 'level2',
            'score' => 30

        ]);
        DB::table('levels')->insert([
            'name' => 'level3',
            'score' => 40

        ]);
        DB::table('levels')->insert([
            'name' => 'level4',
            'score' => 50

        ]);
        DB::table('levels')->insert([
            'name' => 'level5',
            'score' => 60

        ]);
        DB::table('lessons')->insert([
            'name' => 'lesson1',
            'score' => 10,
            'level_id' => 1

        ]);
        DB::table('lessons')->insert([
            'name' => 'lesson2',
            'score' => 10,
            'level_id' => 1

        ]);
        DB::table('lessons')->insert([
            'name' => 'lesson3',
            'score' => 10,
            'level_id' => 1

        ]);
        DB::table('qTypes')->insert([
            'name' => 'type0'

        ]);
        DB::table('qTypes')->insert([
            'name' => 'type1'

        ]);
        DB::table('qTypes')->insert([
            'name' => 'type2'

        ]);
        DB::table('qTypes')->insert([
            'name' => 'type3'

        ]);
        DB::table('qTypes')->insert([
            'name' => 'type4'

        ]);
        DB::table('qTypes')->insert([
            'name' => 'type5'

        ]);
        DB::table('qTypes')->insert([
            'name' => 'type6'

        ]);
        DB::table('qTypes')->insert([
            'name' => 'type7'

        ]);
        // DB::table('questions')->insert([
        //     'contentText' => 'ولد',
        //     'contentImg' => 'boy.png',
        //     'contentAudio' => 'Hello.mp3',
        //     'level_id' => 1,
        //     'qType_id' =>1,
        //     'lesson_id' =>1
        // ]);
        // DB::table('questions')->insert([
        //     'contentText' => 'بنت',
        //     'contentImg' => 'girl.png',
        //     'contentAudio' => 'Hello.mp3',
        //     'level_id' => 1,
        //     'qType_id' =>1,
        //     'lesson_id' =>1
        // ]);
        // DB::table('questions')->insert([
        //     'contentText' => 'ولد يأكل',
        //     'contentImg' => 'boy_eat.png',
        //     'contentAudio' => 'Hello.mp3',
        //     'level_id' => 1,
        //     'qType_id' =>1,
        //     'lesson_id' =>1
        // ]);
        // DB::table('questions')->insert([
        //     'contentText' => 'بنت تشرب',
        //     'contentImg' => 'girl_drink.jpg',
        //     'contentAudio' => 'Hello.mp3',
        //     'level_id' => 1,
        //     'qType_id' =>1,
        //     'lesson_id' =>1
        // ]);
        // DB::table('questions')->insert([
        //     'contentText' => 'بنت تاكل',
        //     'contentImg' => 'girl_eat.png',
        //     'contentAudio' => 'Hello.mp3',
        //     'level_id' => 1,
        //     'qType_id' =>1,
        //     'lesson_id' =>1
        // ]);
        // DB::table('questions')->insert([
        //     'contentText' => 'رجل يشرب',
        //     'contentImg' => 'man_drink.jpg',
        //     'contentAudio' => 'Hello.mp3',
        //     'level_id' => 1,
        //     'qType_id' =>1,
        //     'lesson_id' =>1
        // ]);
        // DB::table('questions')->insert([
        //     'contentText' => 'امرأة',
        //     'contentImg' => 'woman.jpg',
        //     'contentAudio' => 'Hello.mp3',
        //     'level_id' => 1,
        //     'qType_id' =>1,
        //     'lesson_id' =>1
        // ]);
        // DB::table('questions')->insert([
        //     'contentText' => 'بنت تشرب',
        //     'contentImg' => 'girl_drink.jpg',
        //     'contentAudio' => 'Hello.mp3',
        //     'level_id' => 1,
        //     'qType_id' =>2,
        //     'lesson_id' =>1
        // ]);
        // DB::table('questions')->insert([
        //     'contentText' => 'رجل يشرب',
        //     'contentImg' => 'man_drink.jpg',
        //     'contentAudio' => 'Hello.mp3',
        //     'level_id' => 1,
        //     'qType_id' =>3,
        //     'lesson_id' =>1
        // ]);
        // DB::table('questions')->insert([
        //     'contentText' => 'امرأة',
        //     'contentImg' => 'woman.jpg',
        //     'contentAudio' => 'Hello.mp3',
        //     'level_id' => 1,
        //     'qType_id' =>4,
        //     'lesson_id' =>1
        // ]);
        // DB::table('questions')->insert([
        //     'contentText' => 'ولد يأكل',
        //     'contentImg' => 'boy_eat.png',
        //     'contentAudio' => 'Hello.mp3',
        //     'level_id' => 1,
        //     'qType_id' =>5,
        //     'lesson_id' =>1
        // ]);
        // DB::table('questions')->insert([
        //     'contentText' => 'ولد',
        //     'contentImg' => 'boy.png',
        //     'contentAudio' => 'Hello.mp3',
        //     'level_id' => 1,
        //     'qType_id' =>6,
        //     'lesson_id' =>1
        // ]);
        // DB::table('questions')->insert([
        //     'contentText' => 'بنت',
        //     'contentImg' => 'girl.png',
        //     'contentAudio' => 'Hello.mp3',
        //     'level_id' => 1,
        //     'qType_id' =>7,
        //     'lesson_id' =>1
        // ]);


    }
}
