function check_pass()
{
  var password=document.getElementById("new_password").value;
  if (password.length < 8)
  {
       document.getElementById("invalidpass").innerHTML="must be at least 8 characters";
       return false;
   }
  else if (password.search(/[a-z]/i) < 0)
  {
      document.getElementById("invalidpass").innerHTML=" must contain at least one letter.";
      return false;
   }
   else if (password.search(/[0-9]/) < 0)
   {
       document.getElementById("invalidpass").innerHTML=" must contain at least one digit.";
       return false;
   }
   else
   document.getElementById("invalidpass").innerHTML="";
   return true;
}
function check_confirmpass()
{
  if($("#new_password").val()==$("#confirme_password").val())
  return true;
  else {
    document.getElementById("invalidconfirmpass").innerHTML="passwords are not match";
    return false;
  }
}
function change_password()
{
  if(check_pass()==true&&check_confirmpass()==true)
  {
    return true;
  }
  else {
    return false;
  }
}
function submit_form()
{
  var image = document.querySelector("#image");
  if ( /\.(jpe?g|png)$/i.test(image.files[0].name) === false )
   {
   $("#invalid_image").html("Not an image !");
  }
  else {
    $("#invalid_image").html("");
    $("#profile_form").submit();
  }

}
