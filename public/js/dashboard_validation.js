function validateadmin(name)
{
  var illegalChars =  /"[^a-zA-Z0-9\s\u0600-\u06ff\u0750-\u077f\ufb50-\ufc3f\ufe70-\ufefc]"/; // allow letters, numbers, and underscores
      //var illegalChars2 = /\W/;
    if (name.value ==="") {
     document.getElementById("admin_nameinvalid").innerHTML="لم تقم بإدخال اسم المدير";
     return false;}
        else if (illegalChars.test(name.value)) {
           document.getElementById("admin_nameinvalid").innerHTML="الاسم لا يجب ان يحتوي علي رموز و ارقام";
           return false;}
           else {
               middlname.style.background = 'White';
               document.getElementById("invalidusermiddlname").innerHTML="";}
               return true;

}
function validateadminemail()
{
  var x = document.getElementById("admin_email").value;
  var atpos = x.indexOf("@");
  var dotpos = x.lastIndexOf(".");

  if (!(atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)) {
      document.getElementById("admin_emailinvalid").innerHTML=""
      return true;
  }
  else
  {
      document.getElementById("admin_emailinvalid").innerHTML="بريد الكتروني خطأ"
      return false;
  }
}
function validateprojecttitle(project_title)
{
  if (project_title.value ==="") {
   document.getElementById("project_titleinvalid").innerHTML="لم تقم بإدخال اسم المشروع";
   return false;}
   else document.getElementById("project_titleinvalid").innerHTML="";
   return true;

}
function validateproject(project)
{
  if (project.value ==="") {
   document.getElementById("project_desinvalid").innerHTML="لم تقم بإدخال وصف للمشروع";
   return false;}
   else document.getElementById("project_desinvalid").innerHTML="";
   return true;

}
function check_logo(){

var filename = $("#logo").val();

      var extension = filename.split('.').pop();
      if(!filename){
      $("#add_idea").submit();
    }
      else if(extension.toLowerCase() !='jpg' && extension.toLowerCase() !='jpeg' && extension.toLowerCase() !='png' && extension.toLowerCase() !='gif'){
        alert("هذه ليست صورة")
      }
      else
      {
        $("#add_idea").submit();
      }

        }
