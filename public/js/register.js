
function validateUsername() {

    var username =document.getElementById("username").value;
    //var illegalChars =  /"[^a-zA-Z0-9\s\u0600-\u06ff\u0750-\u077f\ufb50-\ufc3f\ufe70-\ufefc]"/; // allow letters, numbers, and underscores
     var illegalChars = /\W/;

    if (username ==="") {
        document.getElementById("invaliduser").innerHTML="please enter you username";
        return false;
      }
    else if ((username.length > 20)) {
        document.getElementById("invalidusername").innerHTML="username is very long";
        return false;}

    else if (illegalChars.test(username.value))
     {
        document.getElementById("invaliduser").innerHTML="username must not has numbers or special character";
        return false;

    }
     else
     {

        document.getElementById("invaliduser").innerHTML="";
        return true;

    }

}

function validateEmail() {
    var x = document.getElementById("email").value;
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");

    if (!(atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)) {
        document.getElementById("invalidEmail").innerHTML=""
        return true;
    }
    else
    {
        document.getElementById("invalidEmail").innerHTML="invalid email";
        return false;
    }

}
function validatepassword()
{
  var password=document.getElementById("password").value;
  if (password.length < 8)
  {
       document.getElementById("invalidpassword").innerHTML="must be at least 8 characters";
       return false;
   }
  else if (password.search(/[a-z]/i) < 0)
  {
      document.getElementById("invalidpassword").innerHTML=" must contain at least one letter.";
      return false;
   }
   else if (password.search(/[0-9]/) < 0)
   {
       document.getElementById("invalidpassword").innerHTML=" must contain at least one digit.";
       return false;
   }
   else
   document.getElementById("invalidpassword").innerHTML="";
   return true;
}
function validate()
{
  if(validateUsername()==true && validatepassword()==true && validateEmail()==true )
  return true;
  else
  return false;
}
