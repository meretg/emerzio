"use strict";

var Dashboard = function () {
	var global = {
		tooltipOptions: {
			placement: "right"
		},
		menuClass: ".c-menu"
	};

	var menuChangeActive = function menuChangeActive(el) {
		$(global.menuClass + " .is-active").removeClass("is-active");
		$(el).addClass("is-active");
	};

	var seatchHoverState = function seatchHoverState(el, eType) {
		var elParent = $(el).parent();

		if (eType == "mouseenter") {
			elParent.addClass("is-hovered");
			return;
		}
		elParent.removeClass("is-hovered");
	};

	var sidebarChangeWidth = function sidebarChangeWidth() {
		var $menuItemsTitle = $("li .menu-item__title");

		$("body").toggleClass("sidebar-is-reduced sidebar-is-expanded");
		$(".hamburger-toggle").toggleClass("is-opened");

		if ($("body").hasClass("sidebar-is-expanded")) {
			$('[data-toggle="tooltip"]').tooltip("destroy");
		} else {
			$('[data-toggle="tooltip"]').tooltip(global.tooltipOptions);
		}
	};

	return {
		init: function init() {
			$(".js-hamburger").on("click", sidebarChangeWidth);
			$(".js-search-btn").on("mouseenter mouseleave", function (e) {
				seatchHoverState(e.currentTarget, e.type);
			});

			$(".js-menu li").on("click", function (e) {
				menuChangeActive(e.currentTarget);
			});

			$('[data-toggle="tooltip"]').tooltip(global.tooltipOptions);
		}
	};
}();

Dashboard.init();

$(document).ready(function(){
//	$(".fa-user").click(function(){
//		$(".user").show();
//		$(".result").hide();
//		$(".courses").hide();
//	})
//	$(".fa-book").click(function(){
//		$(".result").hide();
//		$(".user").hide();
//		$(".courses").show();
//		
//	})
//	$(".fa-line-chart").click(function(){
//		$(".result").show();
//		$(".user").hide();
//		$(".courses").hide();
//	})
})