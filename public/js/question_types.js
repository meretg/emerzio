var right_answer;
var score=0;
var html="";
var arr=[];
var images=[];
var texts=[];
var input_id=[];
var correct_answer=[];
var right_arrangment="";
var draggable_litters=[];
var wrong_questions=[];
var counter=0;
var current_question=-1;
var answer;
var i=-1;
var wrong_questions_index=-1;
var question_counter=0;
var x=0;
var status=0;
function show_questions(current_question,current_status)
{
  fadediv();
  i=current_question;
  i++;


       if(arr[i].qType_id ==1)
       {
         if(arr[i].status==current_status){
           html+='<div class="align-items-center justify-content-center"><h3 class=" text-center">Read and listen to the word(s).</h3><div class="card"><div class="card-body"><div class="row align-items-center justify-content-center"><img src="'+image_source+'/'+arr[i].contentImg+'" class="image align-items-center justify-content-center"></div><div class="row align-items-center justify-content-center"> <audio controls autoplay class="audio align-items-center justify-content-center"><source src="'+image_source+'/'+arr[i].contentAudio+'" type="audio/mpeg">Your browser does not support the audio element.</audio></div></div><div class="footer"><div class=" row align-items-center justify-content-center"><p class=" text-center">'
            +arr[i].contentText+'</p></div></div></div>';
            html+='<div class="row next_btn align-items-center justify-content-center"><a onclick="show_questions(i,status);" class="btn btn-danger next_btn">Next</a></div></div>';
           $("#content").html(html);
           if(question_counter<=arr.length-1){
             question_counter++;
             current_question=i;
           }
         }
         else {
             question_counter++;
           which_case();

         }
     }
          else if(arr[i].qType_id ==2)
         { if(arr[i].status==current_status){
           right_answer=arr[i].id;
           $("#text").fadeOut("slow");
           html+='<div class="align-items-center justify-content-center"><h3 class="text-center">Select the correct option.</h3><div class="row question align-items-center justify-content-center"><p class="question_text text-center">'+arr[i].contentText+'</p></div><div class="row">';
           for(j in arr[i].choices)
           {
             html+='<div class="card"><div class="card-body"><div class="col-md-3 col-xs-12  option_block "> <img src="'+image_source+'/'+arr[i].choices[j].image+
             '" class="question_image align-items-center justify-content-center"></div></div><br><br> <div class="footer flex align-items-center justify-content-center"><input class="options" type="radio" name='+arr[i].contentText+'  value ="'+arr[i].choices[j].imageId+'"> </div></div>';
           }
             html+='</div></div><div class="row align-items-center justify-content-center next_btn"><a  onclick=next($("input:radio:checked").val(),2) class="btn btn-danger next_btn">Next</a></div>';
              $("#content").html(html);
              if(question_counter<=arr.length-1){
                question_counter++;

              current_question=i;}

            }
            else {
              question_counter++;
              which_case();

            }
          }

             else if(arr[i].qType_id ==3)
            {   if(arr[i].status==current_status){
               right_answer=arr[i].id;
              $("#text").fadeOut("slow");
              html+='<h3 class=" text-center">Select the correct option.</h3><div class="row align-items-center justify-content-center"><audio controls autoplay class="audio question_audio"><source src="'+image_source+'/'+arr[i].contentAudio+'" type="audio/mpeg">Your browser does not support the audio element.</audio></div><div class="row">';
              for(j in arr[i].choices)
              {

                html+='<div class="card"><div class="card-body"><div class="col-md-3 option_block"> <img src="'+image_source+'/'+arr[i].choices[j].image+
                '" class="question_image center-block"><br><br></div></div><div class="footer flex align-items-center justify-content-center"><input class="options" type="radio" name='+arr[i].contentText+'  value ="'+arr[i].choices[j].imageId+'"></div></div>';
              }
                html+='</div></div><div class="row align-items-center justify-content-center next_btn"><a  onclick=next($("input:radio:checked").val(),3) class="btn btn-danger next_btn">Next</a></div>';
                 $("#content").html(html);
                 if(question_counter<=arr.length-1){
                   question_counter++;

                 current_question=i;}

               }
               else {
                 question_counter++;
                 which_case();

               }
             }

                else if(arr[i].qType_id ==4)
               {   if(arr[i].status==current_status){
                  right_answer=arr[i].id;
                 $("#text").fadeOut("slow");
                 html+='<h3 class=" text-center">Select the correct option.</h3> <div class="row align-items-center justify-content-center"><img src="'+image_source+'/'+arr[i].contentImg+'" class="image question_audio center-block"></div><div class="row align-items-center justify-content-center">';
                 for(j in arr[i].choices)
                 {
                   html+='<div class="card"><div class="card-body"><div><p class="text-center option_text">'+arr[i].choices[j].text+'</p></div></div><div class="footer flex align-items-center justify-content-center"><input class="options"  type="radio" name='+arr[i].contentText+'  value ="'+arr[i].choices[j].textId+

                   '"></div></div>';
                 }
                   html+='</div><div class="row align-items-center justify-content-center next_btn"><a  onclick=next($("input:radio:checked").val(),4) class="btn btn-danger next_btn">Next</a></div>';
                    $("#content").html(html);
                    if(question_counter<=arr.length-1){
                      question_counter++;
                    current_question=i;}

                  }
                  else {
                    question_counter++;
                    which_case();

                  }
                }

                 else  if(arr[i].qType_id ==5)
                  {   if(arr[i].status==current_status){
                     right_answer=arr[i].id;
                    $("#text").fadeOut("slow");
                    html+='<h3 class=" text-center">Select the correct option.</h3> <div class="row align-items-center justify-content-center"><img src="'+image_source+'/'+arr[i].contentImg+'" class="image"></div><div class="row align-items-center justify-content-center"><audio controls autoplay class="audio question_audio"><source src="'+image_source+'/'+arr[i].contentAudio+'" type="audio/mpeg">Your browser does not support the audio element.</audio></div><div class="row align-items-center justify-content-center">';
                    for(j in arr[i].choices)
                    {
                      html+='<div class="card"><div class="card-body"><div><p class="text-center">'+arr[i].choices[j].text+'</p></div></div><div class="footer flex align-items-center justify-content-center"><input class="options"  type="radio" name='+arr[i].contentText+'  value ="'+arr[i].choices[j].textId+

                      '"></div></div>';
                    }
                      html+='</div><div class="row align-items-center justify-content-center next_btn"><a  onclick=next($("input:radio:checked").val(),5) class="btn btn-danger next_btn">Next</a></div>';
                       $("#content").html(html);
                       if(question_counter<=arr.length-1){
                         question_counter++;

                       current_question=i;}

                     }
                   else {
                     question_counter++;
                     which_case();

                   }}

                   else if (arr[i].qType_id ==6)
                   {
                     if(arr[i].status==current_status){
                       right_answer=arr[i].id;
                       $("#text").fadeOut("slow");

                       for(var x=0;x<arr[i].matcharray.length; x++)
                       {
                         images[x]={
                           index:arr[i].matcharray[x].index,
                           image:arr[i].matcharray[x].image
                         };


                         texts[x]={
                           index:arr[i].matcharray[x].index,
                           text:arr[i].matcharray[x].text
                         };
                       }

                       texts=_.shuffle(texts);
                       html+='<div class="row"><h3 class=" text-center">Type the appropriate picture number for the word inside the box</h3></div><div class="card"><div class="card-body">';

                       for(var y=0;y<images.length;y++)
                       {input_id[y]=texts[y].index;
                         correct_answer[y]="id"+images[y].index;

                         html+='<div class="row match_row"><h6 class="col-md-1 text-center">'+images[y].index+'.</h6><div class=" col-md-6 "><img class="image center-block" src="'+image_source+'/'+images[y].image+
                         '"></div><h5 class="col-md-3  text text-center">'+texts[y].text+'</h5><div class="col-md-2 center-block"><input onchange="input_num();" class="match_inputs center-block" type="text" id="'+texts[y].index+
                         '"></div></div>';

                       }
                       html+='</div></div><div class="row align-items-center justify-content-center"><a style="color:white;" onclick="correctmatch()" class="btn btn-danger next_btn align-items-center justify-content-center">Next</a></div>';

                         $("#content").html(html);
                         if(question_counter<=arr.length-1){
                           question_counter++;

                         current_question=i;}
                       }
                       else {
                         question_counter++;
                         which_case();

                       }}
                       else if(arr[i].qType_id ==7)
                       {
                         if(arr[i].status==current_status)
                         {drag_drop();

                             if(question_counter<=arr.length-1){
                               question_counter++;

                             current_question=i;}
                       }
                       else {
                         question_counter++;
                         which_case();

                       }
                     }
                    //  else if(arr[i].qType_id ==8)
                    //    {
                    //      right_answer=arr[i].contentText;
                    //      html+='<div style="margin-top:2%;"><img src="../images/'+arr[i].contentImg+
                    //      '" class="image"><br><br> <audio controls autoplay><source src="../images/'+arr[i].contentAudio+
                    //      '" type="audio/mpeg">Your browser does not support the audio element.</audio><br><br><input type="text" id="keyboard_text" value=""  dir="rtl" class="keyboardInput">';
                    //    html+='<div class="row"><div class="col-md-6"><input type="button" value="next" onclick=next($("#keyboard_text").val(),8); ></div>';
                    //    $("#content").html(html);
                    //    if(question_counter<=7)
                    //    {
                    //      question_counter++;
                     //
                    //    current_question=i;
                    //  }
                    //    }

}
function fadediv()
{
  html="";
  $("#content").html(html);
}


function next(answer,question_type)
{

  if(right_answer != answer && question_type==4) {

    $("#audio").empty();

     html+='<div class="row align-items-center justify-content-center"><audio controls class="audio question_audio" autoplay id="'+arr[i].id+'"><source src="'+image_source+'/'+arr[i].contentAudio+'" type="audio/mpeg">Your browser does not support the audio element.</audio></div>';
     html+='<div class="row align-items-center justify-content-center"><div class="alert alert-danger alert-dismissible fade show"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><p><strong>oooops!</strong><br> your answer is wrong ,listen to audio then try again</p></div></div>';
     $("#content").html(html);
     counter++;
     if(counter==2)
     {
       arr[i].status++;
       html="";
       html+='<div class="align-items-center justify-content-center"><h3 class=" text-center">Read and listen to the word(s).</h3><div class="card"><div class="card-body"><div class="row align-items-center justify-content-center"><img src="'+image_source+'/'+arr[i].contentImg+'" class="image align-items-center justify-content-center"></div><div class="row align-items-center justify-content-center"> <audio controls autoplay class="audio align-items-center justify-content-center"><source src="/'+arr[i].contentAudio+'" type="audio/mpeg">Your browser does not support the audio element.</audio></div></div><div class="footer"><div class=" row align-items-center justify-content-center"><p class=" text-center">'
        +arr[i].contentText+'</p></div></div></div>';
        html+='<div class="row next_btn align-items-center justify-content-center"><a onclick="which_case();" class="btn btn-danger next_btn">Next</a></div></div>';
       $("#content").html(html);

       counter=0;

       }

     }



    else if(answer==right_arrangment && question_type==7)
    {
      score++;

       html='<div class="row align-items-center justify-content-center"><div class="alert alert-success alert-dismissible fade show"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><p><strong>yaaaay!</strong><br> your answer is right</p></div></div>';
       $("#content").html(html);
      setTimeout(which_case, 1500);

       }

 else if(right_answer.toString() === answer.toString())

{
  score++;

   html='<div class="row align-items-center justify-content-center"><div class="alert alert-success alert-dismissible fade show" ><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><p><strong>yaaaay!</strong><br> your answer is right</p></div></div>';
   $("#content").html(html);
    setTimeout(which_case, 1500);


   }

       else {
         html+='<div class="row align-items-center justify-content-center"><div class="alert alert-danger alert-dismissible fade show"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><p><strong>oooops!</strong><br> your answer is wrong ,you can try again</p></div></div>';
         $("#content").html(html);

if(arr[i].qType_id ==7){
html="";
  setTimeout(drag_drop, 1500);
// drag_drop();
}

counter++;

if(counter==2)
{
 arr[i].status++;

// wrong_questions.push(i);

  html="";

  html+='<div class="align-items-center justify-content-center"><h3 class=" text-center">Read and listen to the word(s).</h3><div class="card"><div class="card-body"><div class="row align-items-center justify-content-center"><img src="'+image_source+'/'+arr[i].contentImg+'" class="image align-items-center justify-content-center"></div><div class="row align-items-center justify-content-center"> <audio controls autoplay class="audio align-items-center justify-content-center"><source src="/'+arr[i].contentAudio+'" type="audio/mpeg">Your browser does not support the audio element.</audio></div></div><div class="footer"><div class=" row align-items-center justify-content-center"><p class=" text-center">'
   +arr[i].contentText+'</p></div></div></div>';
   html+='<div class="row next_btn align-items-center justify-content-center"><a onclick="which_case();" class="btn btn-danger next_btn">Next</a></div></div>';
  $("#content").html(html);

   counter=0;

}
}
}
function which_case()
{

if(question_counter==arr.length)
{
i=0;
question_counter=0;
status++;


show_questions(i,status);
}

else if(question_counter==arr.length-1&&status==1)
{

html='<div class="row  align-items-center justify-content-center"><img style="width:50%; height:50%;" class=" align-items-center justify-content-center" src="'+image_source+'/fireworks.png"></div><div class="row  align-items-center justify-content-center"><h3 class="text text-center">yaaaay you finish this lesson now you can go to the</h3></div> <div calss="row  align-items-center justify-content-center next_btn"><a class="btn next_btn btn-danger align-items-center justify-content-center" style="color:white;" onclick="next_level_request();">Next lesson</a></div>';
$("#content").html(html);

}
else {
show_questions(i,status);
}

}
function next_level_request()
{
  $.ajax({
          url:updateUserLesson,
          type: "post",
          data:{"_token":$('#_token').val(),"lesson_id":id},
           success:function(respons)
           {
             // alert(respons);
            if(respons=='true')
          { window.location=lessons_page; }
            else {

              window.location=levels_page;
            }

          },
           error:function()
           {
             alert("error");
           }
         });


}
function correctmatch()
{
  var number_falses=0;
  for(var s=0;s<input_id.length;s++)
  {
    if($("#"+input_id[s]).val()==input_id[s])
    {
      $(window).scrollTop(0);
      $("#"+input_id[s]).css("border" , "1px solid #17A2B8") ;
      $("#"+input_id[s]).after('<div class="row" style="color:#17A2B8;">True</div>');

    }
 else {

       number_falses++;
      $(window).scrollTop(0);
      $("#"+input_id[s]).val("");
      $("#"+input_id[s]).css("border" , "1px solid #E43B4B");
      $("#"+input_id[s]).after(' <div class="row" style="color:#E43B4B;">False</div>');
    }
  }
  // which_case();
  if(number_falses>0){
  counter++;

  if(counter==2 && number_falses<=4)
  {
    // arr[i].matcharray=_.shuffle(arr[i].matcharray);
     $(window).scrollTop(0);
    html="";
    html+='<div class="row text text-center"><h3>The right answer</h3> </div><div class="row align-items-center justify-content-center"><div class="card"><div class="card-body">';
    for(var m=0;m<arr[i].matcharray.length;m++)
  {
    html+='<div class="row match_row"><div class="col-md-2 text text-center"><h5>'+arr[i].matcharray[m].index+'.</h5></div><div class="col-md-6"><img class="image align-items-center justify-content-center"  src="'+image_source+'/'+arr[i].matcharray[m].image+
    '"></div><div class="col-md-4 text text-center"><h5>'+arr[i].matcharray[m].text+'</h5></div></div>';

  }
    html+='</div></div></div><div class="row align-items-center justify-content-center"><a onclick="which_case();" style="color:white;" class="btn btn-danger next_btn align-items-center justify-content-center">Next</a></div>';
    $("#content").html(html);
arr[i].status++;
    counter=0;


  }

}
else if(number_falses==0) {
  score++;

   html='<div class="row align-items-center justify-content-center"><div class="alert alert-success alert-dismissible fade show" ><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><p><strong>yaaaay!</strong><br> your answer is right</p></div></div>';
   $("#content").html(html);
    setTimeout(which_case, 1500);
}
}
function drag_drop()
{
  right_arrangment=arr[i].contentText;
  html+='<div class="row text-center"><h3>Reorder the word correctly</h3></div><div class="card"><div class="card-body"><div class="row align-items-center justify-content-center"><img src="'+image_source+'/'+arr[i].contentImg+
  '" class="image center-block"></div><div class="row align-items-center justify-content-center"> <audio controls autoplay class="audio question_audio"><source src="'+image_source+'/'+arr[i].contentAudio+'" type="audio/mpeg">Your browser does not support the audio element.</audio></div>';
  for(var a=0;a<arr[i].contentText.length;a++)
  {
    draggable_litters.push(arr[i].contentText[a]);
  }
  html+='<div id="dvSource">';
  draggable_litters=_.shuffle(draggable_litters);
  for(var b=0;b<draggable_litters.length;b++)
  {
    html+='<span>'+draggable_litters[b]+'</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
  }
  draggable_litters=[];
  html+='</div> <div id="dvDest">Drop here</div></div></div> ';
  html+='<div class="row align-items-center justify-content-center"><a style="color:white;" onclick=next($("#dvDest").text(),7); class="btn btn-danger next_btn">Next</a></div>';
    $("#content").html(html);
    $("#dvSource span").draggable({
        revert: true,
        refreshPositions: true,
        drag: function (event, ui) {
            ui.helper.addClass("draggable");
        },
        stop: function (event, ui) {
            ui.helper.removeClass("draggable");
        }
    });
    $("#dvDest").droppable({
        drop: function (event, ui) {
            if ($("#dvDest span").length == 0) {
                $("#dvDest").html("");
            }
            ui.draggable.addClass("dropped");
            $("#dvDest").append(ui.draggable);


        }

    });

}
function input_num()
{
  for(var x=0;x<texts.length;x++)
  {
    if($("#"+texts[x].index).val()>texts.length)
    {
      $("#"+texts[x].index).val("");
    }
  }
}
