
var levels=[];
var lessons=[];
var question_types=[];
function validateuseremail()
{

  var y = document.getElementById("user_email").value;
  var atpos = y.indexOf("@");
  var dotpos = y.lastIndexOf(".");

  if (!(atpos<1 || dotpos<atpos+2 || dotpos+2>=y.length)) {

      document.getElementById("user_emailinvalid").innerHTML="";
      return true;
  }
  else
  {

        document.getElementById("user_emailinvalid").innerHTML="Invalid email";
      return false;
  }
}
function validateUsername_user() {


    var username_user =document.getElementById("user_name").value;
    //var illegalChars =  /"[^a-zA-Z0-9\s\u0600-\u06ff\u0750-\u077f\ufb50-\ufc3f\ufe70-\ufefc]"/; // allow letters, numbers, and underscores
     var illegalChars = /\W/;

    if (username_user==="") {

        document.getElementById("user_nameinvalid").innerHTML="please enter you username";
        return false;
      }
    else if ((username_user.length>20)) {

        document.getElementById("user_nameinvalid").innerHTML="username is very long";
        return false;}

    else if (illegalChars.test(username_user.value))
     {
        document.getElementById("user_nameinvalid").innerHTML="username must not has numbers or special character";
        return false;

    }
     else
     {

        document.getElementById("user_nameinvalid").innerHTML="";
        return true;

    }

}
function validatepassword_user()
{
  var password=document.getElementById("user_password").value;
  if (password.length < 8)
  {
       document.getElementById("user_passwordinvalid").innerHTML="must be at least 8 characters";
       return false;
   }
  else if (password.search(/[a-z]/i) < 0)
  {
      document.getElementById("user_passwordinvalid").innerHTML=" must contain at least one letter.";
      return false;
   }
   else if (password.search(/[0-9]/) < 0)
   {
       document.getElementById("user_passwordinvalid").innerHTML=" must contain at least one digit.";
       return false;
   }
   else
   document.getElementById("user_passwordinvalid").innerHTML="";
   return true;
}
function validateadminemail()
{
  var x = document.getElementById("admin_email").value;
  var atpos = x.indexOf("@");
  var dotpos = x.lastIndexOf(".");

  if (!(atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)) {
      document.getElementById("admin_emailinvalid").innerHTML="";
      return true;
  }
  else
  {
      document.getElementById("admin_emailinvalid").innerHTML="Invalid email"
      return false;
  }
}
function validateUsername() {

    var username =document.getElementById("admin_name").value;
    //var illegalChars =  /"[^a-zA-Z0-9\s\u0600-\u06ff\u0750-\u077f\ufb50-\ufc3f\ufe70-\ufefc]"/; // allow letters, numbers, and underscores
     var illegalChars = /\W/;

    if (username ==="") {
        document.getElementById("admin_nameinvalid").innerHTML="please enter you username";
        return false;
      }
    else if ((username.length > 20)) {
        document.getElementById("admin_nameinvalid").innerHTML="username is very long";
        return false;}

    else if (illegalChars.test(username.value))
     {
        document.getElementById("admin_nameinvalid").innerHTML="username must not has numbers or special character";
        return false;

    }
     else
     {

        document.getElementById("admin_nameinvalid").innerHTML="";
        return true;

    }

}
function validatepassword()
{
  var password=document.getElementById("admin_password").value;
  if (password.length < 8)
  {
       document.getElementById("admin_passwordinvalid").innerHTML="must be at least 8 characters";
       return false;
   }
  else if (password.search(/[a-z]/i) < 0)
  {
      document.getElementById("admin_passwordinvalid").innerHTML=" must contain at least one letter.";
      return false;
   }
   else if (password.search(/[0-9]/) < 0)
   {
       document.getElementById("admin_passwordinvalid").innerHTML=" must contain at least one digit.";
       return false;
   }
   else
   document.getElementById("admin_passwordinvalid").innerHTML="";
   return true;
}
function validateconfirmpass()
{
  if($("#admin_password").val()==$("#admin_password2").val())
  return true;
  else {
    $("#admin_repasswordinvalid").html("Passwords not match");
    return false;
  }
}
function validate()
{
  if(validateUsername()==true && validatepassword()==true && validateEmail()==true && validateconfirmpass()==true )
  {alert("hi");
  return true;
}
  else
  {alert("false");
  return false;
}
}
function display_question_inputs()
{
  $('#select_lesson').html("");
  $('#select_lesson_board').html("");
  $('#select_userlesson').html("");
  $('#edit_userlesson').html("");
  $('#select_lesson').append($("<option selected disabled>Lessons</option>"));
  $('#select_lesson_board').append($("<option selected disabled>Lessons</option>"));
  $('#select_userlesson').append($("<option selected disabled>Lessons</option>"));
  $('#edit_userlesson').append($("<option selected disabled>Lessons</option>"));
 var selected_level=$('#select_level :selected').val();
 var selected_level_board=$('#select_level_board :selected').val();
 var selected_userlevel=$('#select_userlevel :selected').val();
  var selected_userlevel_edit=$('#edit_userlevel :selected').val();
  for(var i=0;i<lessons.length;i++)
  {
    if(lessons[i].level_id==selected_level)
    {
    $('#select_lesson').append($("<option></option>")
                       .attr("value",lessons[i].id)
                       .text(lessons[i].name));
    $("#lesson_input").css("display", "block");
    }
    if(lessons[i].level_id==selected_level_board)
    {
      $('#select_lesson_board').append($("<option></option>")
                               .attr("value",lessons[i].id)
                               .text(lessons[i].name));
      $("#lessons").css("display", "block");
    }
    if(lessons[i].level_id==selected_userlevel)
    {
      $('#select_userlesson').append($("<option></option>")
                               .attr("value",lessons[i].id)
                               .text(lessons[i].name));
    }
    if(lessons[i].level_id==selected_userlevel_edit)
    {
      $('#edit_userlesson').append($("<option></option>")
                               .attr("value",lessons[i].id)
                               .text(lessons[i].name));
    }

    }


}
function display_question()
{
  $("#question_inputs").css("display", "block");
}
function checkextension() {
  var image = document.querySelector("#image");
  var audio = document.querySelector("#audio");
  // alert(image);
  if ( /\.(jpe?g|png)$/i.test(image.files[0].name) === false )
   {
   $("#invalid_image").html("Not an image !");
  }
  else {
    $("#invalid_image").html("");
  }
  if ( /\.(MP3|WAV|WMA)$/i.test(audio.files[0].name) === false )
   {
   $("#invalid_audio").html("Not an audio !");
  }
  else {
    $("#invalid_audio").html("");
  }
}
