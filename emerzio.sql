-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 18, 2018 at 05:33 PM
-- Server version: 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `emerzio`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--


-- --------------------------------------------------------

--
-- Table structure for table `levels`
--



--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`id`, `name`, `score`, `created_at`, `updated_at`) VALUES
(1, 'level1', '20.00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--


-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--


--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_100000_create_password_resets_table', 1),
('2017_11_16_094019_create_admins_table', 1),
('2017_12_05_144349_create_levels_table', 1),
('2014_10_12_000000_create_users_table ', 2),
('2017_12_05_144603_create_logs_table', 3),
('2017_12_05_144845_create_question_types_table', 3),
('2017_12_05_144536_create_questions_table', 4),
('2017_12_05_144819_create_user_answers_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--



-- --------------------------------------------------------

--
-- Table structure for table `qTypes`
--


--
-- Dumping data for table `qTypes`
--

INSERT INTO `qTypes` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'type0', NULL, NULL),
(2, 'type1', NULL, NULL),
(3, 'type2', NULL, NULL),
(4, 'type3', NULL, NULL),
(5, 'type4', NULL, NULL),
(6, 'type5', NULL, NULL),
(7, 'type6', NULL, NULL),
(8, 'type7', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--


--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `contentText`, `contentImg`, `contentAudio`, `level_id`, `qType_id`, `created_at`, `updated_at`) VALUES
(1, 'ولد', 'boy.png', 'Hello.mp3', 1, 1, NULL, NULL),
(2, 'بنت', 'girl.png', 'Hello.mp3', 1, 1, NULL, NULL),
(3, 'ولد يأكل', 'boy_eat.png', 'Hello.mp3', 1, 1, NULL, NULL),
(4, 'بنت تشرب', 'girl_drink.jpg', 'Hello.mp3', 1, 1, NULL, NULL),
(5, 'بنت تاكل', 'girl_eat.png', 'Hello.mp3', 1, 1, NULL, NULL),
(6, 'رجل يشرب', 'man_drink.jpg', 'Hello.mp3', 1, 1, NULL, NULL),
(7, 'امرأة', 'woman.jpg', 'Hello.mp3', 1, 1, NULL, NULL),
(8, 'بنت تشرب', 'girl_drink.jpg', 'Hello.mp3', 1, 2, NULL, NULL),
(15, 'رجل يشرب', 'man_drink.jpg', 'Hello.mp3', 1, 3, NULL, NULL),
(16, 'أمرأه', 'woman.jpg', 'Hello.mp3', 1, 4, NULL, NULL),
(17, 'ولد يأكل', 'boy_eat.png', 'Hello.mp3', 1, 5, NULL, NULL),
(18, 'ولد', 'boy.png', 'Hello.mp3', 1, 6, NULL, NULL),
(19, 'بنت', 'girl.png', 'Hello.mp3', 1, 7, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `userAnswers`
--



-- --------------------------------------------------------

--
-- Table structure for table `users`
--


--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `username`, `password`, `image`, `DOB`, `gender`, `country`, `score`, `level_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '', '', 'ghjh', '$2y$10$TayxZgzTYeCKfMuV90sjNufv8JaHTDSsQLgxc5Eq2QRRgP1Lr6l7y', 'profile.jpg', '0000-00-00', 'male', NULL, 0, NULL, NULL, '2018-01-15 10:52:41', '2018-01-15 10:52:41'),
(4, '', 'a1@hotmail.com', 'r', '$2y$10$nSFxUiypXOEwE8BhXKpyxeGxjIdv2x3Xx81POIB13QyyOQTCNJJHe', 'profile.jpg', '0000-00-00', 'male', NULL, 0, NULL, NULL, '2018-01-16 08:16:44', '2018-01-16 08:16:44'),
(5, '', 'admin@codex.net', 'yh', '$2y$10$pZST3uWgwqR1vr9r6HXmFOf0PP0Lph/pKUXPj7tpopmS5bQCHpH5i', 'profile.jpg', '0000-00-00', 'male', NULL, 0, NULL, NULL, '2018-01-17 06:28:29', '2018-01-17 06:28:29'),
(6, '', 'a@hotmail.com', 'rehab', '$2y$10$sngD.OfhVIiA5W/Aa/Khl.fe1IvCsk7unAEmnppWoPquiB0tcBSG6', 'woman-drinking-alkaline-water.png', '0000-00-00', 'male', NULL, 0, NULL, 'nQkbE0m67dpXJtDswf5XUyvSYPPN0GYCLwBYTZT1u8IO2iaV6OJbBgJqhX4O', '2018-01-17 07:06:07', '2018-01-18 13:21:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
-- ALTER TABLE `admins`
--   ADD PRIMARY KEY (`id`);
--
-- --
-- -- Indexes for table `levels`
-- --
-- ALTER TABLE `levels`
--   ADD PRIMARY KEY (`id`);
--
-- --
-- -- Indexes for table `logs`
-- --
-- ALTER TABLE `logs`
--   ADD PRIMARY KEY (`id`),
--   ADD KEY `logs_user_id_foreign` (`user_id`);
--
-- --
-- -- Indexes for table `password_resets`
-- --
-- ALTER TABLE `password_resets`
--   ADD KEY `password_resets_email_index` (`email`),
--   ADD KEY `password_resets_token_index` (`token`);
--
-- --
-- -- Indexes for table `qTypes`
-- --
-- ALTER TABLE `qTypes`
--   ADD PRIMARY KEY (`id`);
--
-- --
-- -- Indexes for table `questions`
-- --
-- ALTER TABLE `questions`
--   ADD PRIMARY KEY (`id`),
--   ADD KEY `questions_level_id_foreign` (`level_id`),
--   ADD KEY `questions_qtype_id_foreign` (`qType_id`);
--
-- --
-- -- Indexes for table `userAnswers`
-- --
-- ALTER TABLE `userAnswers`
--   ADD PRIMARY KEY (`id`),
--   ADD KEY `useranswers_user_id_foreign` (`user_id`),
--   ADD KEY `useranswers_question_id_foreign` (`question_id`);
--
-- --
-- -- Indexes for table `users`
-- --
-- ALTER TABLE `users`
--   ADD PRIMARY KEY (`id`),
--   ADD UNIQUE KEY `users_username_unique` (`username`),
--   ADD UNIQUE KEY `users_email_unique` (`email`),
--   ADD KEY `users_level_id_foreign` (`level_id`);
--
-- --
-- -- AUTO_INCREMENT for dumped tables
-- --
--
-- --
-- -- AUTO_INCREMENT for table `admins`
-- --
-- -- ALTER TABLE `admins`
-- --   MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
-- --
-- -- AUTO_INCREMENT for table `levels`
-- --
-- -- ALTER TABLE `levels`
-- --   MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
-- --
-- -- AUTO_INCREMENT for table `logs`
-- --
-- -- ALTER TABLE `logs`
-- --   MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
-- -- --
-- -- AUTO_INCREMENT for table `qTypes`
-- --
-- ALTER TABLE `qTypes`
--   MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
-- --
-- -- AUTO_INCREMENT for table `questions`
-- --
-- ALTER TABLE `questions`
--   MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
-- --
-- -- AUTO_INCREMENT for table `userAnswers`
-- --
-- ALTER TABLE `userAnswers`
--   MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
-- --
-- -- AUTO_INCREMENT for table `users`
-- --
-- ALTER TABLE `users`
--   MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
-- --
-- -- Constraints for dumped tables
-- --
--
-- --
-- -- Constraints for table `logs`
-- --
-- ALTER TABLE `logs`
--   ADD CONSTRAINT `logs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
--
-- --
-- -- Constraints for table `questions`
-- --
-- ALTER TABLE `questions`
--   ADD CONSTRAINT `questions_level_id_foreign` FOREIGN KEY (`level_id`) REFERENCES `levels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
--   ADD CONSTRAINT `questions_qtype_id_foreign` FOREIGN KEY (`qType_id`) REFERENCES `qTypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
--
-- --
-- -- Constraints for table `userAnswers`
-- --
-- ALTER TABLE `userAnswers`
--   ADD CONSTRAINT `useranswers_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
--   ADD CONSTRAINT `useranswers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
--
-- --
-- -- Constraints for table `users`
-- --
-- ALTER TABLE `users`
--   ADD CONSTRAINT `users_level_id_foreign` FOREIGN KEY (`level_id`) REFERENCES `levels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
--
-- /*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
-- /*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
-- /*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
