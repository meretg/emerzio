<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class question extends Model
{
    public $table="questions";
    public function questionType(){
        return $this->belongsTo('App\questionType');
     }
    public function level(){
        return $this->belongsTo('App\level');
     }
     public function lesson(){
         return $this->belongsTo('App\lesson');   
      }

}
