<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(
[
	'prefix' => LaravelLocalization::setLocale(),
	'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
],
function()
{
	/** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/
	Route::get('/', function()
	{
		return View::make('index');
	});
	Route::get('/emerzio_online', function()
	{
		return View::make('emerzio_online');
	});
	Route::get('/level1/{id}', function()
	{
		return View::make('level1');
	});
	Route::get('/admin_login', function()
	{
		return View::make('admin_login');
	});
	Route::get('/admin_dashboard', function()
	{
		return View::make('admin_dashboard');
	});
	Route::get('/user_dashboard', function()
	{
		return View::make('user_dashboard');
	});

	Route::get('/questions_dashboard', function()
	{
		return View::make('questions_dashboard');
	});
	Route::post('/users', function()
	{
		return View::make('users_dashboard');
	});
    // Route::get('/lesson/{id}', function () {
    //     return view('lesson');
    // });

});

//Route::post('adminlogin','AdminLoginController@login');

Route::get('lesson/{id}','QuestionController@getLessons');
//user controller
//Route::get('userDetails/{id}','UserController@userDetails');
Route::get('/profile',[
	'as' => 'profile', 'uses' => 'UserController@userDetails'

]);
Route::post('updatePassword','UserController@updatePassword');
Route::post('updateImage','UserController@updateImage');
Route::get('addUser/{email}/{username}/{password}','UserController@addUser');
Route::post('updateUser','UserController@updateUser');
//Route::get('deleteUser/{id}','UserController@deleteUser');
Route::post('signIn','UserController@userlogin');
//admins
Route::post('addAdmin','adminController@addAdmin');
Route::post('editAdmin','adminController@editAdmin');
Route::post('deleteAdmin','adminController@deleteAdmin');
Route::get('/en/admin_dashboard','adminController@viewAdmins');
Route::get('viewAdmin','adminController@viewAdmin');
Route::get('viewAdmins','adminController@viewAdmins');
Route::get('admin_logout','adminController@logout');
Route::post('admin_login','adminController@admin_login');
/////////////////////
//users

Route::get('/en/users','adminController@viewUsers');
//Route::get('viewUsers','adminController@viewUsers');
Route::post('addUser','adminController@addUser');
Route::get('editUser','adminController@editUser');
Route::post('deleteUser','adminController@deleteUser');
Route::get('User','adminController@User');
//////////////////////////////
//LEVELS
Route::post('addLevel','adminController@addLevel');
Route::post('editLevel','adminController@editLevel');
Route::get('Level','adminController@Level');
Route::post('deleteLevel','adminController@deleteLevel');
Route::get('levels_dashboard','adminController@viewLevels');
//////////////////
//LESSONS
Route::post('addLesson','adminController@addLesson');
Route::post('editLesson','adminController@editLesson');
Route::post('deleteLesson','adminController@deleteLesson');
Route::get('lessons_dashboard','adminController@viewLessons');
Route::post('viewLevel','adminController@viewLevel');
Route::get('Lesson','adminController@Lesson');

//////////////////////////////
//questions
Route::post('viewLevelsLessons','adminController@viewLevelsLessons');
Route::post('viewQuestions','adminController@viewQuestions');
//Route::post('view_Q','adminController@view_Q');
Route::post('deleteQuestion','adminController@deleteQuestion');

//Route::post('editQuestion','adminController@editQuestion');
//Route::get('viewQuestions/{lesson_id}/{level_id}','adminController@viewQuestions');
// Route::post('/signIn',[
// 	'as' => 'signIn', 'uses' => 'UserController@userlogin'
// ]);
Route::get('signOut',['as' => 'logout', 'uses' => 'UserController@userlogout']);
Route::post('updateUserLevel','UserController@updateUserLevel');
Route::post('updateUserLesson','UserController@updateUserLesson');
Route::get('updateUserLesson/{lesson_id}','UserController@updateUserLesson');
Route::post('signUp','UserController@addUser');
//admincontroller

Route::get('viewUser/{userId}','adminController@viewUser');
Route::post('addQuestion','adminController@addQuestion');
Route::post('uploadImage','adminController@uploadImage');
Route::get('updateQuestion/{questionId}/{contentText}/{contentImg}/{contentAudio}/{level_id}/{qType_id}','adminController@updateQuestion');

Route::get('getQuestion/{questionId}','adminController@getQuestion');

Route::get('emerzio_online','UserController@passedUserLevel');
Route::get('getUsersLevels','UserController@getUsersLevels');
//Route::get('viewQuestions','AdminController@viewQuestions');
//Route::get('viewQuestions','AdminController@viewQuestions');
Route::auth();

Route::get('/home', 'HomeController@index');

Route::get('getAllLevels','UserController@getAllLevels');
Route::get('currentUserLevel/{id}','UserController@currentUserLevel');
// Route::prefix('admin')->group(function(){
// 	Route::get('/login','Auth/AdminLoginController@showLoginForm')->name('admin.login');
// 	Route::post('/login','Auth/AdminLoginController@login')->name('admin.login.submit');
// 	Route::get('/', 'AdminController@index')->name('admin.dashboard');
// });

Route::get('questionLevel/{id}','QuestionController@questionLevel');

Route::get('getQuestionByLevel/{id}','QuestionController@getQuestionByLevel');
Route::post('/questionLevel','QuestionController@questionLevel');

Route::get('ImageText/{id}/{level}','QuestionController@ImageText');


Route::get('questionText/{id}/{level}','QuestionController@questionText');
Route::get('userLevelLessons/{id}','UserController@userLevelLessons');

Route::auth();

//Route::post('/login', 'adminController@login');
//Route::get('logout/', 'auth\AdminLoginController@logout');
// Route::group(['middleware' => 'IsAdmin'], function () {
//     Route::get('admin', 'adminController@login');
// });


Route::auth();

Route::get('/home', 'HomeController@index');

Route::auth();

Route::get('/home', 'HomeController@index');
