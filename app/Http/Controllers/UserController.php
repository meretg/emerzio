<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;



use App\Http\Requests;
use App\user;
use App\level;
use App\lesson;
use Hash;
use Auth;
use Crypt;
use View;

//use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Input;;
use Image;
use Session;


class UserController extends Controller
{

    public function showLoginForm(){
        return view('user.login');
    }
    public function userlogin(Request $request){
        $error="user name or password is wrong";
       // $this->validator($request->all());
        // $this->validate($request,[

        //      'username'=>'required|username',
        //      'password'=>'required'



        //      ]
        //      );
        if(Auth::attempt(['username'=>$request->username,'password'=>$request->password])){
            $user= user::where('username',$request->username)->first();

            //return $user;
         View::share(['user'=>$user]);
         return view('index',['user'=>$user]);
        }
        // else{
        //   Session::flash('flash_message', 'user name or password is wrong');
        //  //return $error;
        //  //echo("fgjfg");
      //  return redirect () -> URL('signIn');
        //   return redirect()->back();
        //  //return view('index',compact($error));
        //  //return view('index');
        //
        // }
        //return json_encode($error);
        Session::flash('flash_message',$error);
        return redirect()->back();
    }



    public function userLogOut ()
    {
        Auth::logout();
        return redirect ('/') ;
    }


    public function userDetails()
    {
       $userId=Auth::user()->id;
        $user= user::findOrFail($userId);
        //return json_encode($user);
        return view('profile',compact ('user'));
    }

    public function addUser(Request $request)
    {

     $message="successfully registeration you must login now!";
      // $validator=  $this->validate($request,[
      //
      //   'username'=>'required|unique:users,username||Regex:/^([A-Za-z])/',
      //   'password'=>'required|unique:users,password| min:8',
      //   'email' => 'required|email|unique:users'
      //
      //
      //
      //   ]
      //   );
        $validator = \Validator::make($request->all(), ['username' => 'required|unique:users,username||Regex:/^([A-Za-z])/', 'email' => 'required|email|unique:users',
        'password'=>'required|| min:8']);

    if ($validator->fails()) {
        $message="The username has already been taken.";

      Session::flash('flash_message',$message);
     return redirect()->back();

    }

        else{
          $newuser=new user;
          //$newuser->name=$request->name;
          $newuser->email=$request->email;
          $newuser->username=$request->username;
          $newuser->password=Hash::make($request->password);
          // $newuser->image=$request->image;
          // $newuser->DOB=$request->DOB;
          // $newuser->gender=$request->gender;
          // $newuser->country=$request->country;
          // $newuser->score=$request->score;
          $newuser->save();
  }
    Session::flash('flash_message',$message);
  return redirect()->back();

    }
    public function updateUser(Request $request)
    {
       $userId= $request->id ;
       $user= user::findOrFail($userId);

       // $user->name=$request->name;
       // $user->email=$request->email;

       //if($request->name){
           $user->username=$request->username;
       //}

       //if($request->email){
           $user->email=$request->email;

       //}
      // if($request->password){
         $user->password=Hash::make($request->password);

       //}

       $user->save();

       return redirect ()-> route ('profile',$user->id);

   }
   public function updatePassword(Request $request){
     $message="your password changed successfully";

     $userId=Auth::user()->id;
    //$userId=$request->id;
    $user=user::findOrFail($userId);
   // check old password
     if (Hash::check($request->oldPassword, $user->password)){
       $user->password=Hash::make($request->newPassword);
       //$user->password=Hash::make($request->newPassword);
       $user->save();


       Session::flash('flash_message',$message);
     return redirect()->back();
    }
     else{
       $error="current password is wrong !";
       Session::flash('flash_message',$error);
     return redirect()->back();
     }
  }

    public function updateImage(Request $request){
      //$file =  $request->hasFile('image');

      $file = Input::file('image');

        $valid = 0;

        //if(isset($file))
        if($request->hasFile('image') && $request->file('image')->isValid())
        {
          $valid = 1;

          $destinationPath = 'images/'; // upload path
          $extension = $request->image->getClientOriginalExtension();
          $fileSize = $file->getClientSize();
          if( (strcasecmp($extension,"jpg") !=0) && (strcasecmp($extension,"jpeg") !=0) && (strcasecmp($extension,"png") !=0) && (strcasecmp($extension,"gif") !=0))
          {
            //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $valid = 0;
          }
        }
        if($valid)
          {
              //$fileName = rand(11111,99999).'.'.$extension; // renaming image
              $file = Image::make(Input::file('image')->getRealPath());
              //$file = Image::make($file);

              // Check file size < 15MB
              if ($fileSize > 15728640)
              {
                  $file->resize(120,75);
              }

              $fileName = time() . '.' . $extension;
              //$file->move($destinationPath, $fileName);
              $file->save($destinationPath . $fileName);
            }
            $user= user::findOrFail($request->id);
            $user->image= $fileName;
            $user->save();


      // if($request->hasFile('image'))
      // {
      //   return json_encode("sss");
      // }
      // else {
      //
      //     return json_encode("ff");
      //
      // }
     return redirect ()-> route ('profile',$request->id);

    }
    public function deleteUser(Request $request)
    {
        $userId= $request->id ;
        $user= user::findOrFail($userId);
        $user->delete();
        return json_encode(true);
    }
    public function getAllLevels(){

        $levelNames=level::pluck('name');
        return json_encode($levelNames);


    }
    public function currentUserLevel(Request $request){
        $userId=$request->id;
        $levelId=user::where('id',$userId)->pluck('level_id');
        if($levelId){
          $currentLevel=level::where('id',$levelId)->pluck('name');
          return json_encode($currentLevel);

        }
        else{
          return json_encode("first time");
        }


    }




//to show user levels
    public function passedUserLevel(){
        // $array=array();
         $i=0;
        // $userId=$request->id;
      $userId=Auth::user()->id;
      $user=user::where('id',$userId)->first();
      $userLevel=user::findOrFail($userId);
         $userLevels=level::all();

          foreach($userLevels as $level){
         $passedLevels=new \stdClass;
         if($user->level_id==Null){
           $user->level_id=0;

         }

         if($userLevel->level_id >= $level->id){
           $passedLevels->levelId=$level->id;
           $passedLevels->passed="passed";
           $passedLevels->name=$level->name;
           $passedLevels->score=$level->score;

             }
             else{
               $passedLevels->levelId=$level->id;
                $passedLevels->passed="notPassed";
               $passedLevels->name=$level->name;
               $passedLevels->score=$level->score;

            }
              $levels[$i++]= $passedLevels;
        }
         return view('emerzio_online',['levels'=>$levels]);
      // return($user->lesson_id);
    }
    public function updateUserLevel($level_id){

          $userId=Auth::user()->id;
      //   //  //$levelId=$request->levelId+1;
           $user=user::findOrFail($userId);

         if($user->level_id==Null){
          $user->level_id=1;

           $user->save();
       //return view('emerzio_online',$user->id);
       }
     elseif($user->level_id<$level_id){
       $user->level_id=$level_id;
       $user->save();
          //return view('emerzio_online',$user->id);
       }

    //return view('emerzio_online',['user'=>$user]);
         return json_encode(  True);
     }
     public function updateUserLesson(Request $request){

           $userId=Auth::user()->id;
           $endLevel="finish";

       //   //  //$levelId=$request->levelId+1;
            $user=user::findOrFail($userId);
          //  $userLevelLessons=$this->userLevelLessons($user->level_id,$request->lesson_id);
          if($user->level_id==Null){
           $level_id=1;

          //  $user->save();
        //return view('emerzio_online',$user->id);
        }
        else{
           $level_id=$user->level_id;
        }


          $levelLessons=lesson::where('level_id',$level_id)->max('id');


              if($levelLessons==$request->lesson_id){
                $this->updateUserLevel($user->level_id);
                 $user->lesson_id=$request->lesson_id;
                $user->save();

               return json_encode('finish');
              }


          if($user->lesson_id==Null){
            // if($levelLessons==1){
            //   $user->level_id=1;
            // }
           $user->lesson_id=1;

            $user->save();
        //return view('emerzio_online',$user->id);
        }

      elseif($user->lesson_id<$request->lesson_id){
        $user->lesson_id=$request->lesson_id;
        $user->save();
           //return view('emerzio_online',$user->id);
        }

     //return view('emerzio_online',['user'=>$user]);
         return json_encode(  True);
      }
    public function getUsersLevels(){
        $users=user::all();
        $array=array();
        foreach( $users as $user ){

            $levelId=user::where('id',$user->id)->pluck('level_id');
            $level=level::where('id',$levelId)->pluck('name');
            $array[$user->username]=$level;
        }
        return json_encoded($array);

    }
    // public function userLevelLessons(Request $Request){
    //   $levelLessons=lesson::where('level_id',$Request->id)->get();
    //   $lessonId=$Request->lesson;
    //     foreach( $levelLessons as $levelLesson ){
    //       if($levelLesson==$lessonId){
    //         $this->updateUserLevel($Request->id)
    //       }
    //     }
    //   return json_encode($levelLessons);
    //
    // }



}
