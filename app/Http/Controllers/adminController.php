<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Admin;
use App\user;
use App\level;
use App\lesson;
use App\questionType;
use App\question;
use Hash;
use Auth;
use Session;
use View;
use Storage;
use Image;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Input;;

class adminController extends Controller
{
   private  $admin_check;
  public function __construct()
    {
 //$this->middleware('Auth:Admin');
$this->admin_check=Auth::guard('Admin')->check();
    }
  public function admin_login(Request $request){
      // echo($request->email);
    $errors="wrong email or password";
    //$message="you are logged as  Admin";
      $admins=admin::paginate(5);
    //  $newPassword=Hash::make($request->password);
        $this->admin_check=Auth::guard('Admin')->attempt(['email'=>$request->email,'password'=>$request->password]);
        foreach($admins as $admin){
          if($admin->email==$request->email){
            $admin->IsAdmin=True;
          }
          else{
            $admin->IsAdmin=False;
          }
        }

  if($this->admin_check){

  //return View::make('admin_dashboard',compact('admins'));

  return redirect('admin_dashboard')->with(['admins'=>$admins]);
  //  return redirect()->route('/admin_dashboard');
//  return view ('admin_dashboard');
       }
       else {
        Session::flash('flash_message',$errors);
        return redirect()->back();

       }
    }

  public function logout()
  {

   Auth::guard('Admin')->logout();
$this->admin_check= Auth::guard('Admin')->check();
//return json_encode($check);
//Session::flush();
//if($this->admin_check){
    return redirect ('admin_login');
//}

      //  Session::flush();
  }
  public function addAdmin(Request $request){
    if(!$this->admin_check){
      return redirect ('admin_login');
    }

      $error="duplicate name";
      $check=admin::where('username',$request->username)->first();

        $admins= Admin::paginate(5);
        $id=Auth::guard('Admin')->id();
        foreach($admins as $admin){
          if($admin->id==$id){
            $admin->IsAdmin=True;
          }
          else{
            $admin->IsAdmin=False;
          }
        }
      if($check){
    Session::flash('flash_message',$error);
      return redirect('admin_dashboard')->with(['admins'=>$admins]);
//return json_encode($check);

      }
  else{
    $newAdmin=new Admin;
    $newAdmin->username=$request->username;
    $newAdmin->email=$request->email;
    $newAdmin->password=Hash::make($request->password);
    $newAdmin->save();
    $admins= Admin::paginate(5);
    $id=Auth::guard('Admin')->id();
    foreach($admins as $admin){
      if($admin->id==$id){
        $admin->IsAdmin=True;
      }
      else{
        $admin->IsAdmin=False;
      }
    }

return redirect('admin_dashboard')->with(['admins'=>$admins]);
  }


//return json_encode($check);

}
public function viewAdmin(Request $request){
  if($this->admin_check){
     $adminId=$request->id;
     $admin=Admin::findOrFail($adminId);
     return json_encode($admin);
   }
}
public function editAdmin(Request $request){

   $adminId=$request->id;
   $newAdmin= Admin::findOrFail($adminId);
   $newAdmin->username=$request->username;
   $newAdmin->email=$request->email;
   $newAdmin->password=Hash::make($request->password);
   $newAdmin->save();
   $admins= Admin::paginate(5);
   $id=Auth::guard('Admin')->id();
   foreach($admins as $admin){
     if($admin->id==$id){
       $admin->IsAdmin=True;
     }
     else{
       $admin->IsAdmin=False;
     }
   }

return redirect('admin_dashboard')->with(['admins'=>$admins]);
//return json_encode($adminId);
}

public function deleteAdmin(Request $request){

  $adminId= $request->id;
  $admin= Admin::findOrFail($adminId);
  $admin->delete();
  $admins= Admin::paginate(5);
  $id=Auth::guard('Admin')->id();
  foreach($admins as $admin){
    if($admin->id==$id){
      $admin->IsAdmin=True;
    }
    else{
      $admin->IsAdmin=False;
    }
  }

//return json_encode(true);
return redirect('admin_dashboard')->with(['admins'=>$admins]);
}

public function viewAdmins(){
  //$error="you must login as Admin first";
 //Session::flush();
 if(!$this->admin_check){
   return redirect ('admin_login');
 }
$check=Auth::guard('Admin')->check();

if($check){
  $admins=admin::paginate(5);
  $id=Auth::guard('Admin')->id();
  //Session::flush();
  foreach($admins as $admin){
    if($admin->id==$id){
      $admin->IsAdmin=True;
    }
    else{
      $admin->IsAdmin=False;
    }
  }
//Session::flush();
  return View::make('admin_dashboard',compact('admins'));


}
else {

  //Session::flush();
 //Session::flash('flash_message',$error);
  return redirect ('admin_login');
}
}


public function viewUsers(){
  $i=0;
    $users=user::paginate(5);
foreach($users as $user){
  $level=level::where('id',$user->level_id)->first();
  $lesson=lesson::where('id',$user->lesson_id)->first();
  if($user->level_id==Null){
    $user->level="empty";

  }
  else{
    $user->level=$level->name;
  }
  if($user->lesson_id==Null){
        $user->lesson="empty";
  }
  else{

    $user->lesson=$lesson->name;
  }

  $users[$i++]=$user;
}
 return View::make('users_dashboard',compact('users'));
 //return redirect('en/users')->with(['users'=>$users]);
  //return View::make('users_dashboard',['lessons_levels'=>$lessons_levels]);
//return View::make('users_dashboard',['lessons_levels'=>$lessons_levels] );

//return json_encode($users);

  }

  public function addUser(Request $request){
    $newuser=new user;

    $newuser->email=$request->email;
    $newuser->username=$request->username;
    $newuser->password=Hash::make($request->password);
    $newuser->level_id=$request->level_id;
    $newuser->lesson_id=$request->lesson_id;

    $newuser->save();
    $this->viewUsers();
  //  $users=user::all();

   return redirect()->back();
  //  return json_encode(true);

  }
  public function User(Request $request){
    $newUser= user::findOrFail($request->id);
    return json_encode($newUser);
  }



public function editUser(Request $request){
        $userId=$request->id;
       //
      $newuser= user::where('id',$userId)->first();
      $newuser->username=$request->username;
     $newuser->email=$request->email;
if($request->level_id){
  $newuser->level_id=$request->level_id;
}
      if($request->lesson_id){
      $newuser->lesson_id=$request->lesson_id;
    }
      $newuser->save();
      $this->viewUsers();
   return redirect()->back();

     }

  public function deleteUser(Request $request){
      $userId= $request->id;
      $user= user::findOrFail($userId);
      $user->delete();
      $this->viewUsers();
   return redirect()->back();

  }
  public function addLevel(Request $request){
    $newlevel=new level;
    $newlevel->name=$request->name;
    $newlevel->save();
    $levels=level::paginate(5);
    //return View::make('levels_dashboard',compact('levels'));
    return redirect('levels_dashboard')->with(['levels'=>$levels]);

  }
  public function editLevel(Request $request){
     $levelId=$request->id;
     $newlevel= level::findOrFail($levelId);

     $newlevel->name=$request->name;
     $newlevel->save();
     $levels=level::paginate(5);
     //return View::make('levels_dashboard',compact('levels'));
    // return json_encode(true);
return redirect('levels_dashboard')->with(['levels'=>$levels]);
}
  public function Level(Request $request){
    $levelId=$request->id;
    $level=level::findOrFail($levelId);
    $name=$level->name;
    return json_encode($name);
  }
public function deleteLevel(Request $request){
  $levelId= $request->id;
  $level= level::findOrFail($levelId);
  $level->delete();
  $levels=level::paginate(5);
  //return View::make('levels_dashboard',compact('levels'));
  return redirect('levels_dashboard')->with(['levels'=>$levels]);
}
public function viewLevels(){
  $levels=level::paginate(5);
  return View::make('levels_dashboard',compact('levels'));
//return json_encode($levels);
//return view('levels_dashboard',['levels'=>$levels]);
//return view('levels_dashboard', $levels);
//return view('levels_dashboard')->with('levels');
}
public function viewLevel(Request $request){
  $lessons=lesson::where('level_id',$request->level_id)->get();
  $levels=level::paginate(5);
  //return View::make('lessons_dashboard',compact('lessons'));
return json_encode($lessons);
}
public function addLesson(Request $request){
  $newLesson=new lesson;
  $newLesson->name=$request->name;

  $newLesson->level_id=$request->level_id;
  $newLesson->save();
  $lessons=lesson::all();
  //$levels=level::all();
  //return View::make('lessons_dashboard');
  return json_encode(True);

}
public function editLesson(Request $request){
   $lessonId=$request->id;
   $newLesson= lesson::findOrFail($lessonId);
   $newLesson->name=$request->name;

   $newLesson->save();
   $lessons=lesson::all();
   $levels=level::all();
   return View::make('lessons_dashboard',compact('lessons','levels'));

}
public function deleteLesson(Request $request){
$lessonId= $request->id;
$levels=level::all();
$lesson= lesson::findOrFail($lessonId);
$lesson->delete();
$lessons=lesson::all();
//$this->viewLessons();
//  View::share(['levels'=>$levels]);
//  View::share(['lessons'=>$lessons]);
// return redirect()->back();
return View::make('lessons_dashboard',compact('lessons','levels'));
}
public function viewLessons(){
//  $lessons=lesson::where('level_id',$request->level_id);
  $levels=level::all();
  return View::make('lessons_dashboard',compact('levels'));


}
 public function Lesson(Request $request){
  $lessonId=$request->id;
  $lesson=lesson::findOrFail($lessonId);
  $name=$lesson->name;
  return json_encode($name);
}

public function viewLesson(Request $request){
  $questions=lesson::where('lesson_id',$request->id);
  return View::make('lessons_dashboard',compact('questions'));

}
 public function viewLevelsLessons(){
   $lessons=lesson::all();
   $levels=level::all();
  $types=questionType::all();
  $lessons_levels=new \stdClass;
  $lessons_levels->levels=$levels;
  $lessons_levels->lessons=$lessons;
  $lessons_levels->types=$types;
  //return View::make('questions_dashboard',compact('lessons_levels'));
  // json_encode(array('lessons'=>$lessons,'levels'=>$levels);
//return view('questions_dashboard', compact('lessons', 'levels'));
//Response::json(array('lessons'=>$lessons,'levels'=>$levels));
//return [$lessons,$levels];
//return view('questions_dashboard', [ 'lessons_levels'=>$lessons_levels ]);
//return view('greeting')->with('name', 'Victoria');
//return view('questions_dashboard',['lessons_levels'=>$lessons_levels]);
return json_encode($lessons_levels);
}

public function addQuestion(Request $request){
            $file =  $request->hasFile('image');


       $newquestion=new question;
       $newquestion->contentText=$request->contentText;
       $file = Input::file('image');

         $valid = 0;

         //if(isset($file))
         if($request->hasFile('image') && $request->file('image')->isValid())
         {
           $valid = 1;

           $destinationPath = 'images/'; // upload path
           $extension = $request->image->getClientOriginalExtension();
           $fileSize = $file->getClientSize();
           if( (strcasecmp($extension,"jpg") !=0) && (strcasecmp($extension,"jpeg") !=0) && (strcasecmp($extension,"png") !=0) && (strcasecmp($extension,"gif") !=0))
           {
             //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
             $valid = 0;
           }
         }
        //  if($valid)
        //    {
               //$fileName = rand(11111,99999).'.'.$extension; // renaming image
               $file = Image::make(Input::file('image')->getRealPath());
               //$file = Image::make($file);

               // Check file size < 15MB
               if ($fileSize > 15728640)
               {
                   $file->resize(120,75);
               }

               $fileName = time() . '.' . $extension;
               //$file->move($destinationPath, $fileName);
               $file->save($destinationPath . $fileName);
             //}
            //  $user= user::findOrFail($request->id);
            //  $user->image= $fileName;
       $newquestion->contentImg=$fileName;

        $newquestion->contentAudio=$request->audio;
       $audio = Input::file('audio');
       //$request->audio->store('images');

         $valid = 0;

         //if(isset($file))
         if($request->hasFile('audio') && $request->file('audio')->isValid())
         {
           $valid = 1;

           $destinationPath = 'images/'; // upload path
           $extension = $request->audio->getClientOriginalExtension();
           $fileSize = $audio->getClientSize();
            $audioFile = time() . '.' . $extension;
           Storage::put(
                        $audioFile,
                        file_get_contents($request->file('audio')->getRealPath())
                    );

           if( (strcasecmp($extension,"MP3") !=0) && (strcasecmp($extension,"WAV") !=0) && (strcasecmp($extension,"WMA") !=0))
           {
             //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
             $valid = 0;


           }
         }

               if ($fileSize > 15728640)
               {
                   $audio->resize(120,75);
               }


          $newquestion->contentAudio=$audioFile;
       //
       $newquestion->level_id=$request->level_id;
       $newquestion->lesson_id=$request->lesson_id;
       $newquestion->qType_id=$request->qType_id;
       //
       $newquestion->save();
      $questions=question::paginate(5);
       //
       //
        return View::make('questions_dashboard',compact('questions'));
   //return json_encode($audioFile);

   }


   public function deleteQuestion(Request $request){
       $questionId= $request->id ;
       $question= question::findOrFail($questionId);
       $question->delete();
       $questions=question::all();
// return View::make('questions_dashboard',compact('questions'));
  return view('questions_dashboard');
   }

 public function viewQuestions(Request $request){
   $i=0;
   $level_id=$request->level_id;
   $lesson_id=$request->lesson_id;
   $level=level::where('id',$level_id)->pluck('name');
   $lesson=lesson::where('id',$lesson_id)->pluck('name');
   $question=question::where([['level_id', $level_id],['lesson_id',$lesson_id]])->get();
   //$question->setPath('/questions_dashboard');
   //$question->paginate(5);
   foreach($question as $k){
     $questions= new \stdClass;
     $type=questionType::where('id',$k->qType_id)->pluck('name');
     $questions->type=$type;
     $questions->level=$level;
     $questions->lesson=$lesson;
     $questions->questions=$k;
     $array[$i++]=$questions;
   }
  // return View::make('questions_dashboard',compact('questions'));
  //$array->paginate(5);
//  if ($request->ajax()) {
  // $this->view_Q($array);
    return json_encode($array);
//  }

//$view =View::make('questions_dashboard')->render();


//return redirect('questions_dashboard')->with(['array'=>$array]);
 }
 // public function view_Q($array){
 //   return View::make('questions_dashboard', array('array' => $array))->render();
 //
 // }

}
