<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\question;
//use App\userAnswer;
use App\level;
use App\questionType;
use App\lesson;
use App\user;
use DB;
use Auth;
class QuestionController extends Controller
{

    public function questionText ($id,$text,$lesson){
      $i=0;
      $final=array();
      $correctanswer=new \stdClass;
      $questions=question::where([['lesson_id',$lesson],['qType_id',1],['contentText','<>',$text]])->get();
      foreach( $questions as  $question){
          $choices=new \stdClass;
          $choices->image=$question->contentImg;
          $choices->imageId=$question->id;
          $final[$i++]= $choices;
      }
      shuffle($final);
      $final=array_slice($final,0,3);
     $correct=question::where('id',$id)->first();
     //echo ($correct);
     $correctanswer->image=$correct->contentImg;
     $correctanswer->imageId=$correct->id;
     $final[$i]=$correctanswer;
     shuffle($final);
     return $final;



     }
     public function questionImg ($id,$text,$lesson){
          $i=0;
          $final=array();
          $correctanswer=new \stdClass;
          $questions=question::where([['lesson_id',$lesson],['qType_id',1],['contentText','<>',$text]])->get();
          foreach( $questions as  $question){
              $choices=new \stdClass;
              $choices->text=$question->contentText;
              $choices->textId=$question->id;
              $final[$i++]= $choices;
          }
              shuffle($final);
              $final=array_slice($final,0,3);
          $correct=question::where('id',$id)->first();
          //echo ($correct);
          $correctanswer->text=$correct->contentText;
          $correctanswer->textId=$correct->id;
          $final[$i]=$correctanswer;
          shuffle($final);
      return $final;

     }
     public function ImageText ($id,$lesson){
        $i=0;
       $x=1;
       $final=array();
       $correctanswer=new \stdClass;
       $questions=question::where([['lesson_id',$lesson],['id','<>',$id]])->get();
       foreach( $questions as  $question){
           $image_text=new \stdClass;
           $image_text->text=$question->contentText;

           $image_text->image=$question->contentImg;
           //$image_text->imageId=$question->id;
          //$image_text->index=$x++;
           $final[$i++]=$image_text;


       }

       //shuffle($final);
       $final=array_slice($final,0,4);
      // $correct=question::where('id',$id)->first();
      // $correctanswer->text=$correct->contentText;
      // $correctanswer->image=$correct->contentImg;
      //$correctanswer->index=$x++;
      //$final[$i]=$correctanswer;
      shuffle($final);
      foreach($final as $item){
          $item->index=$x++;
      }
      //shuffle($final);
       return $final;

      }

  public function questionLevel (Request $request){
      $i=0;
      $x=0;
      $z=0;
      $choices_text=array();
      $lessonId=$request->id;
      $question_array=array();
      $imagess=array();
      $questions=question::where('lesson_id',$lessonId)->get();
      //$images=question::where('level_id',$levelId)->get();
      //$texts=question::where('level_id',$levelId)->get();
      //$images=question::where([['level_id',$levelId],['id','<>',$question->id])->pluck('contentImg');

     // echo($images);
      foreach ( $questions as  $question){
          //$images=question::where([['level_id',$levelId],['id','<>',$question->id]])->get();

           // echo ($images);

          $questionType=questionType::where('id', $question->qType_id)->first();
          $question_choices=new \stdClass;







          //echo($questionType);

          //unset($question->contentImg);
          //echo($images);
          //$texts=question::where([['level_id',$levelId],['qType_id',$question->qType_id]])->get();
          //echo($texts);
          switch($questionType->name){
              case 'type0' :
             // echo($questionType->name);

             $question_choices=$question;

              $question_choices=$question;

              $question_choices->questionType=$questionType->name;
              $question_choices->status=0;

              break;

              case 'type1':
              //$question_choices=new \stdClass;

              $question_choices=$question;
              $question_choices->questionType=$questionType->name;
              $question_choices->choices= $this->questionText($question->id,$question->contentText,$lessonId);
               $question_choices->status=0;

              break;
              case 'type2':
              //$question_choices=new \stdClass;
              $question_choices=$question;
              $question_choices->questionType=$questionType->name;
              $question_choices->choices= $this->questionText($question->id,$question->contentText,$lessonId);
              $question_choices->status=0;
              break;
              case 'type3':
              //$question_choices=new \stdClass;
              $question_choices=$question;
              $question_choices->questionType=$questionType->name;
              $question_choices->choices= $this->questionImg($question->id,$question->contentText,$lessonId);
               $question_choices->status=0;
              break;
              case 'type4':
              //$question_choices=new \stdClass;
              $question_choices=$question;
              $question_choices->questionType=$questionType->name;
              $question_choices->choices= $this->questionImg($question->id,$question->contentText,$lessonId);
              $question_choices->status=0;
              break;
              case 'type5':

                  $question_choices=$question;
                  $question_choices->questionType=$questionType->name;

                 $question_choices->matcharray=$this->ImageText($question->id,$question->lesson_id);
                 $question_choices->status=0;
              break;
              case 'type6':
              //$question_choices=new \stdClass;

              $question_choices=$question;
              $question_choices->questionType=$questionType->name;
              $question_choices->status=0;
              break;
              case 'type7':
              //$question_choices=new \stdClass;

              $question_choices=$question;
              $question_choices->questionType=$questionType->name;
              $question_choices->status=0;
              break;





          }

              $question_array[$x++]=$question_choices;



      }
      //shuffle($question_array);

        return  $question_array;
         //var_dump ($question_array);
  }

public function getLessons (Request $request){
   $levelId=$request->id;
  // $lessons=lesson::where('level_id', $levelId)->get();
  //return json_encode($lesson);
  //return view('lesson',['lessons'=>$lessons]);

$i=0;
 $lessons=array();
// $userId=$request->id;
$userId=Auth::user()->id;

$user=user::findOrFail($userId);
if($user->lesson_id==Null){
  $user->lesson_id=0;

}
$userLessons=lesson::where('level_id', $levelId)->get();

 foreach($userLessons as $lesson){
$passedLessons=new \stdClass;
if($user->lesson_id >= $lesson->id){
  $passedLessons->lessonId=$lesson->id;
  $passedLessons->passed="passed";
  $passedLessons->name=$lesson->name;


    }
    else{
    $passedLessons->lessonId=$lesson->id;
    $passedLessons->passed="notPassed";
    $passedLessons->name=$lesson->name;


   }
     $lessons[$i++]= $passedLessons;
}
return view('lesson',['lessons'=>$lessons]);
//return json_encode($lessons);


}
}
