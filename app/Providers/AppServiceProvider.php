<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use  Blade;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::setEchoFormat('e(utf8_decode(%s))');
        view()->composer('*', function($view) {
            $view->with('user', auth()->user());
        // $view->with('admin', auth()->guard('Admin'));
        });
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
