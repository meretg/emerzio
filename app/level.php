<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class level extends Model
{
    public $table="levels";
    public function question(){
        return $this->hasMany('App\question');
     }
     public function lesson(){
         return $this->hasMany('App\lesson');   
      }
}
