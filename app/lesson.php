<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class lesson extends Model
{
  public $table="lessons";
  public function question(){
      return $this->hasMany('App\question');
   }
   public function level(){
      return $this->belongsTo('App\level');
   }
}
