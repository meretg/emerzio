<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class userAnswer extends Model
{ 
    public $table="userAnswers"; 	   
    public function user(){     
        return $this->belongsTo('App\user');   
     }
     public function question(){     
         return $this->belongsTo('App\question');   
      }
}
