<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user extends Model
{
//  use AuthenticableTrait;
    public $table="users";
    protected $fillable = [
        'name',
        'email',
        'username',
        'password',
        'image',
        'DOB',
        'gender',
        'country',
        'score'
    ];
    public function level(){
       return $this->belongsTo('App\level');
    }
    public function lesson(){
       return $this->belongsTo('App\lesson');
    }
    public function log(){
        return $this->hasMany('App\log');
     }

}
