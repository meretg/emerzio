<!DOCTYPE html>
<html>
    <head>
      <meta http-equiv="content-type" content="text/html";charset="utf-8" />

        <title>Emerzio</title>

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link href="{{ asset('/img/icon(512x512).png')}}" rel="apple-touch-icon" sizes="96x96">
        <link href="{{ asset('/img/icon(512x512).png')}}" rel="icon" sizes="96x96" type="image/png">
        <link href="{{ asset('/img/icon(512x512).png')}}" rel="icon" sizes="32x32" type="image/png">
        <meta content="{{ asset('/img/icon(512x512).png')}}" name="msapplication-TileImage">

        <link rel="stylesheet" href="{{ asset('/css/theme/owl.carousel.min.css')}}">
        <link rel="stylesheet" href="{{ asset('/css/theme/owl.theme.default.min.css')}}">
        <link rel="stylesheet" href="{{ asset('/css/theme/animate.min.css')}}">
        <link rel="stylesheet" href="{{ asset('/css/theme/app.css')}}">


        <link href='http://fonts.googleapis.com/css?family=Gochi+Hand|Arvo:400,700' rel='stylesheet' type='text/css'>
        <!-- Bootstrap core CSS -->

      <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
          <!-- Primary CSS styles -->
          <link href="{{ asset('/css/newstyle.css')}}" rel="stylesheet" />
          <!-- Masterslider CSS styles -->


      	<link href="http://code.jquery.com/ui/1.8.24/themes/blitzer/jquery-ui.css" rel="stylesheet"
      	type="text/css" />
      	<link rel="stylesheet" type="text/css" href="http://www.arabic-keyboard.org/keyboard/keyboard.css">
      	<link href="http://code.jquery.com/ui/1.8.24/themes/blitzer/jquery-ui.css" rel="stylesheet"
      	type="text/css" />
</head>
<body>
			<div class="bar-holder">
					@if(Session::has('flash_message'))
    <div class="alert alert-info align-items-center justify-content-center" style="width:500px;">
        {{ Session::get('flash_message') }}
    </div>
@endif
@if($errors->any())
    <div class="alert alert-danger align-items-center justify-content-center" style="width:500px;">
        @foreach($errors->all() as $error)
            <p>{{ Session::get('flash_message') }}</p>
        @endforeach
    </div>
@endif
<nav class="navbar navbar-expand-lg navbar-light" data-toggle="affix" id="mainNav" >
      <div class="container">
			<button class="navbar-toggler navbar-toggler-center  mx-auto py-3 my-2 " type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                  Menu
                  <i class="fa fa-bars"></i>
                </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="nav navbar-nav">
					<li class="nav-item"><a class="nav-link" href="{{ url('/') }}">Home</a></li>
					<li class="nav-item"><a class="nav-link" href="#">About us</a></li>
					<li class="nav-item"><a class="nav-link" href="#">Contact</a></li>
					@if(Auth::check())
					<li class="nav-item"><a class="nav-link" href="{{ url('emerzio_online') }}">Emerzio online</a></li>

					<li class="nav-item"><img class="profile_info nav-link" id="profilepic" src="{{ asset('/images')}}/{{ $user->image }}" ></li>
					<li class="nav-item" style="padding-top: 7px;"><div class="nav-link" id="profilename"><a  href="{{ url('profile') }}" >{{$user->username}}</a></div></li>

					<li class="nav-item"><a href="{{ route('logout') }}" class="btn btn-info" id="logout_btn" >logout</a></li>
					@else
					<li class="nav-item"><a href="#" class="btn btn-info nav-link" id="login_btn" data-toggle="modal" data-target="#myModallogin" >Login</a></li>
					<li class="nav-item"><a href="#" class="btn btn-info nav-link" id="register_btn" data-toggle="modal" data-target="#myModalregister">Register</a></li>
					@endif
				</ul>
    </div>
  </div>
</nav>
	</div>

			<div class="modal fade " id="myModallogin" role="dialog">
				<div class="modal-dialog formstyle ">

					<!-- Modal content-->
					<div class="modal-content formstyle   ">
						<div class="modal-header ">
              <h4>Login</h4>
							<button type="button" class="close" data-dismiss="modal">&times;</button>


						</div>
						<div class="modal-body ">
							<div class="login-pop">

							<div id="loginBox">
								<form id="loginForm" action="{{ URL::to('signIn') }}" method="post">
										<fieldset id="body">
											<fieldset>

													<input placeholder="Username..." type="text" class="" required name="username" id="loginusername">
													@if ($errors->has('login-username'))
											<span class="help-block">
													{{ $errors->first('login-username') }}
											</span>
									@endif

											</fieldset>
											<fieldset>

												<input placeholder="Your password..." type="password" class="" required name="password" id="loginpassword">
												@if ($errors->has('login-password'))
											 <span class="help-block">
													 {{ $errors->first('login-password') }}
											 </span>
									 @endif
											 </fieldset>
                       <div class="row align-items-center justify-content-center">
											<input class="btn btn-info" value="Login" type="submit">
                    </div>
                      <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">

										</fieldset>

							 </form>
						</div>
						</div>
						</div>

					</div>

				</div>
			</div>
			<div class="modal fade" id="myModalregister" role="dialog" data-backdrop="static" >

				<div class="modal-dialog formstyle ">

					<!-- Modal content-->
					<div class="modal-content formstyle">
						<div class="modal-header">
              <h4>Register</h4>
							<button type="button" class="close " data-dismiss="modal">&times;</button>


						</div>
						<div class="modal-body">

							<div class="register-pop">

							<div id="registerBox">
								<form id="registerForm" action="{{ URL::to('signUp') }}" method="post">
										<fieldset id="registerbody">
											<fieldset>

													<input onblur="validateUsername();" required id="username" name="username" placeholder="Username..." type="text">
													<span id="invaliduser"></span>
											</fieldset>
											<fieldset>

													<input onblur="validateEmail();" id="email" name="email" placeholder="Your email..." type="email">
													<span id="invalidEmail"></span>
											</fieldset>
											<fieldset>

												<input onblur="validatepassword();" id="password" required name="password" placeholder="Your password..." type="password">
												<span id="invalidpassword"></span>
											 </fieldset>
                       <div class="row align-items-center justify-content-center">
											<input class="btn btn-info" value="Register" type="submit" onclick="return validate();">
                      <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
										</fieldset>

							 </form>

						</div>
						</div>
						</div>

					</div>

				</div>
			</div>

  @yield('content')

		<!-- FOOTER
            =================-->
            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="footer-add">
                                <img src="img/icon(512x512).png" alt="Logo" class="img-fluid" style="width:45%; height:40%;">
                                <div class="address"><i class="fa fa-map-marker"></i> Koramangala Ejipura 212, Bahi, Bangalore-560045, INDIA</div>
                                <div class="phone">Phone: <i class="fa fa-phone"> (+91) 1234567890</i></div>
                                <div class="email">Email: <i class="fa fa-envelope"> info@school.com</i></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="useful-link">
                                <h3>Quick Links</h3>
                                <ul>
                                    <li><i class="fa fa-angle-right"></i> <a href="about.html">About</a></li>
                                    <li><i class="fa fa-angle-right"></i> <a href="academics.html">Academics</a></li>
                                    <li><i class="fa fa-angle-right"></i> <a href="gallery.html">Gallery</a></li>
                                    <li><i class="fa fa-angle-right"></i> <a href="contact.html">Contact us</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="recent-activity">
                                <h3>Recent Activity</h3>
                                <div class="latest-news">
                                  <img src="img/news/news-2-small.jpg" alt="" class="img-fluid">
                                  <p>We achive first position in india country.</p>
                                  <small>12/06/2018</small>
                                </div>
                                <div class="latest-news">
                                  <img src="img/news/news-3-small.jpg" alt="" class="img-fluid">
                                  <p>Our student at first position in india country.</p>
                                  <small>12/06/2018</small>
                                </div>
                            </div>
                      </div>
                    </div>
                </div>
                <div class="container-fluid footer-b text-center">
                    <small>Copyrights &copy; 2018 Design by <a href="https://boostraptheme.com/">Boostraptheme</a></small>
                </div>
            </footer>



            <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
            <script language="javascript" src="{{ asset('/js/jquery-1.11.0.min.js')}}"></script>
            <!-- Easing core JavaScript -->
            <script language="javascript" src="{{ asset('/js/jquery.easing.1.3.js')}}"></script>
            <!-- Bootstrap core JavaScript -->
            <script language="javascript" src="{{ asset('/js/bootstrap.min.js')}}"></script>
            <!-- Master slider core JavaScript -->
            <script language="javascript" src="{{ asset('/js/masterslider.min.js')}}"></script>
            <!-- Master slider staff core JavaScript -->
            <script language="javascript" src="{{ asset('/js/masterslider.staff.carousel.dev.js')}}"></script>
            <!-- WOW core JavaScript -->
             <script language="javascript" src="{{ asset('/js/wow.min.js')}}"></script>
             <!-- Waypoints core JavaScript -->
            <script language="javascript" src="{{ asset('/js/waypoints.min.js')}}"></script>
             <!-- Underscore core JavaScript -->
             <script language="javascript" src="{{ asset('/js/underscore-min.js')}}"></script>
             <!-- jQuery Backstretch core -->
             <script language="javascript" src="{{ asset('/js/jquery.backstretch.min.js')}}"></script>
             <!-- jQuery color core JavaScript -->
             <script language="javascript" src="{{ asset('/js/jquery.animation.js')}}"></script>
             <!-- Isotope core JavaScript -->
             <script language="javascript" src="{{ asset('/js/jquery.isotope.min.js')}}"></script>
            <!-- Stellar core JavaScript -->
             <script language="javascript" src="{{ asset('/js/jquery.stellar.min.js')}}"></script>
             <!-- Contact core JavaScript -->
             <script language="javascript" src="{{ asset('/js/jquery.contact.min.js')}}"></script>
             <!-- NiceScroll core Javascript -->
             <script language="javascript" src="{{ asset('/js/jquery.nicescroll.min.js')}}"></script>
             <!-- Retina core JavaScript -->
             <script language="javascript" src="{{ asset('/js/retina-1.1.0.min.js')}}"></script>
             <!-- Nivo Slider JavaScript -->
             <script language="javascript" src="{{ asset('/js/jquery.nivo.slider.pack.js')}}"></script>
             <!-- Video core JavaScript -->
             <script language="javascript" src="{{ asset('/js/video.js')}}"></script>
             <!-- OWL Carousel core JavaScript -->
             <script language="javascript" src="{{ asset('/js/owl.carousel.min.js')}}"></script>
             <!-- twitterfeed core JavaScript -->
             <script language="javascript" src="{{ asset('/js/jquery.twitterfeed.js')}}"></script>
             <!-- Lightbox core JavaScript -->
             <script language="javascript" src="{{ asset('/js/lightbox.min.js')}}"></script>
            <script type="text/javascript" src="{{ asset('/js/jquery.bxslider.min.js')}}"></script>
            <script type="text/javascript" src="{{ asset('/js/jquery.placeholder.js')}}"></script>
            <script type="text/javascript" src="{{ asset('/js/main.js')}}"></script>
            <script type="text/javascript" src="{{ asset('/js/register.js')}}"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

            <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
            <script>
            var image_source='{{ asset("/images")}}/';
            </script>

        <script src="{{ asset('/js/theme/bootstrap.min.js')}}"></script>
        <script src="{{ asset('/js/theme/jquery.easing.min.js')}}"></script>
        <script src="{{ asset('/js/theme/owl.carousel.min.js')}}"></script>
        <script src="{{ asset('/js/theme/wow.min.js')}}"></script>
        <script src="{{ asset('/js/theme/app.js')}}"></script>
        <!--Start of Tawk.to Script-->
            <script type="text/javascript">
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5a6d77fc4b401e45400c7419/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
            })();
            </script>
</body>
</html>
