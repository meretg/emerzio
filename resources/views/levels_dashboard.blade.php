@extends("admin_master")
@section("content")
<section >
      <div class="container panel panel-default panel-body">
<hr>
<h2 class="col-xs-6 ">Levels</h2>
<button class="col-xs-2 btn btn-success pull-right add_admin" data-toggle="modal" data-target="#addlevelModal">New level</button>
<span class="clearfix"></span>
<hr>

 @if(Session::has('flash_message'))
    <div class="alert alert-info">
        {{ Session::get('flash_message') }}
    </div>
@endif
@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif
<div class="modal fade" id="addlevelModal" role="dialog">
<div class="modal-dialog" >

<!-- Modal content-->

<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Add level</h4>
  </div>
  <div class="modal-body">

     <form class="form-horizontal" enctype="multipart/form-data" action = "{{ action('adminController@addLevel') }}"  method = "post">
<div class="form-group" style="margin-left:1px;" >
  <label class="control-label col-sm-3" >Level name</label>
<div class="col-sm-6">
<input type="text" class="form-control" id="level_name" name = "name" required >
</div>
</div>
<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
<div class="form-group">
<div class=" savebutton">
<button type="submit" class="btn btn-default"  id="add_save" >Save</button>
</div>
</div>

</div>
</div>
</form>
</div>
</div>
<div class="responsive">
<table class="table">
    <thead>
      <tr >
        <th>Level name</th>
        <th>Edit</th>
        <th>Delete</th>
      </tr>
    </thead>
    <tbody>
    @foreach($levels as $level)
        <tr >
            <td>{{$level->name}} <br/></td>
            <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editlevelModal"  onclick = "edit_level('{{$level->id}}');" id="admin_Edit" >Edit</button></td>
          <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deletelevelModal"  onclick = "delete_level('{{$level->id}}');" id="admin_delete" >Delete</button></td>
        </tr>
  @endforeach
    </tbody>
  </table>
  {!! $levels->render()!!}
</div>
</div>
<div class="modal fade" id="editlevelModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit this level</h4>
      </div>
      <div class="modal-body">
          <form class="form-horizontal" action = "{{ action('adminController@editLevel') }}" method = "post">

<div class="form-group">
  <label class="control-label col-sm-2" >Level name</label>
  <div class="col-sm-10">
    <input type="text" class="form-control" id="levelname" name="name">
  </div>
</div>

  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="id" id="editlevel_id">
<div class="form-group">
  <div class=" col-sm-12">
    <button  id="save_update"  type="submit" onclick="this.form.submit()"  class="btn btn-default pull-right"data-dismiss="modal" >Save</button>
  </div>
</div>
      </form>
    </div>

  </div>
</div>

</div>

<div class="modal fade" id="deletelevelModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Are you sure ?</h4>

      </div>
      <div class="modal-body">
       <strong style="color:red;">
           You will delete all information about this level
       </strong>
      </div>
      <div class="modal-footer">

        <form class="form-horizontal" action = "{{ action('adminController@deleteLevel') }}" method = "post">
          <input type="hidden" name="id" id="level_id">
          <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
          <button type="button" class="btn btn-default" data-dismiss="modal" onclick="this.form.submit()">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </form>


      </div>
    </div>

  </div>
</div>
</section>
<script>
    var levelId;
    var editlevel_id;
    function delete_level (id)
    {
      levelId = id;


      $("#level_id").val(levelId);
    }
    function edit_level(id)
{
          editlevel_id=id;
          $("#editlevel_id").val(id);
          $.ajax({
            url: "{{ URL::to('Level') }}",
            type: "get",
            dataType: 'json',
            data: {"_token":$('#_token').val(),"id":id},
            success: function(response)
            {
               $("#levelname").val(response);

               },


            error: function () {

                alert("error");

            }
            });
}
  </script>

@stop
