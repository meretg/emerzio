@extends("master")
@section("content")
<!-- <div class="progress">
  <div  id="progress-bar" class="progress-bar bg-info"></div>
</div> -->
<div class="row align-items-center justify-content-center question_block">
<div id="content">


  <input type="hidden" value="{{$user->level_id}}" id="levelid">
<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
</div>
</div>


<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="{{ asset('/js/question_types.js')}}"></script>

<script>

var parts = window.location.pathname.split('/');
var id = parts.pop() || parts.pop();
var updateUserLesson = "{{ URL::to('updateUserLesson') }}";
var lessons_page="{{url('lesson')}}"+"/"+id;
var levels_page="{{ url('emerzio_online') }}";

$(window).load(function get_questions()
{
  $("#question").hide();
  $(".question_block").show();
  $.ajax({
            url: "{{ URL::to('questionLevel') }}",
            type: "post",
            data:{"_token":$('#_token').val(),"id":id},
             dataType: 'json',
             success:function(data)
             {
                for(i in data)
                {
                  arr.push(data[i]);
                }
              show_questions(current_question,status);
            },
             error:function()
             {
               alert("error");
             }
           });
});


</script>
@stop
