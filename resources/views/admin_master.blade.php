<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Emerzio-Admin</title>
    <link id="favicon" rel="icon" type="image/png" href="{{ asset('/images/icon(512x512).png')}}">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset('/css/AdminLTE.css') }}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="{{ asset('/css/skins/skin-blue.css') }}" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="{{ asset('/plugins/iCheck/square/blue.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/css/admin_style.css') }}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

      <!-- Logo -->
      <a href="{{ url('/') }}" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->

        <span class="logo-lg"><img src="{{ asset('/images/icon(modified).png')}}" style="width=15px;height:15px;">  Emerzio</span>
          <!-- logo for regular state and mobile devices -->

          <img src="{{ asset('/images/icon(modified).png')}}" style="width=15px;height:15px;">
      </a>

      <!-- Header Navbar -->
      <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
              <span class="sr-only">{{ trans('adminlte_lang::message.togglenav') }}</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="pull-right ">
            <div id="logout_button">

            <div>
                <a href = "{{ url('admin_logout') }}" class="btn btn-danger">Logout</a>
              </div>

        </div>
          </div>
      </nav>
  </header>

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">

          <!-- Sidebar user panel (optional) -->


          <!-- search form (Optional) -->

          <!-- /.search form -->

          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">


              <li><a href="{{ url('admin_dashboard') }}"class="v"><i class='fa fa-user'></i> <span>Admins</span></a></li>
              <li><a href="{{ url('en/users') }}"><i class='fa fa-users'></i> <span>Users</span></a></li>
              <li class="treeview">
                  <a href="#"><img src="{{ asset('/images/icon(modified).png')}}" style="width:15px; height:15px;"> <span>Emerzio online</span> <i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                      <li><a href="{{ url('levels_dashboard') }}">Levels</a></li>
                      <li><a href="{{ url('lessons_dashboard') }}">Lessons</a></li>
                      <li><a href="{{ url('questions_dashboard') }}">Questions</a></li>
                  </ul>
              </li>
          </ul><!-- /.sidebar-menu -->
      </section>
      <!-- /.sidebar -->
  </aside>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">



        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->

            @yield('content')

        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>

        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane active" id="control-sidebar-home-tab">
                <h3 class="control-sidebar-heading">{{ trans('adminlte_lang::message.recentactivity') }}</h3>
                <ul class='control-sidebar-menu'>
                    <li>
                        <a href='javascript::;'>
                            <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">{{ trans('adminlte_lang::message.birthday') }}</h4>
                                <p>{{ trans('adminlte_lang::message.birthdaydate') }}</p>
                            </div>
                        </a>
                    </li>
                </ul><!-- /.control-sidebar-menu -->

                <h3 class="control-sidebar-heading">{{ trans('adminlte_lang::message.progress') }}</h3>
                <ul class='control-sidebar-menu'>
                    <li>
                        <a href='javascript::;'>
                            <h4 class="control-sidebar-subheading">
                                {{ trans('adminlte_lang::message.customtemplate') }}
                                <span class="label label-danger pull-right">70%</span>
                            </h4>
                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                            </div>
                        </a>
                    </li>
                </ul><!-- /.control-sidebar-menu -->

            </div><!-- /.tab-pane -->
            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">{{ trans('adminlte_lang::message.statstab') }}</div><!-- /.tab-pane -->
            <!-- Settings tab content -->
            <div class="tab-pane" id="control-sidebar-settings-tab">
                <form method="post">
                    <h3 class="control-sidebar-heading">{{ trans('adminlte_lang::message.generalset') }}</h3>
                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            {{ trans('adminlte_lang::message.reportpanel') }}
                            <input type="checkbox" class="pull-right" {{ trans('adminlte_lang::message.checked') }} />
                        </label>
                        <p>
                            {{ trans('adminlte_lang::message.informationsettings') }}
                        </p>
                    </div><!-- /.form-group -->
                </form>
            </div><!-- /.tab-pane -->
        </div>
    </aside><!-- /.control-sidebar

    <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
    <div class='control-sidebar-bg'></div>

    <!-- Main Footer -->
    <footer class="main-footer center-block">

        <strong>Copyright &copy;<a href="#">Emerzio.com</a>.</strong>
    </footer>

</div><!-- ./wrapper -->

<!-- iCheck -->
<script src="{{ asset('/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/js/app.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/js/admin_validation.js') }}" type="text/javascript"></script>


</body>
</html>
