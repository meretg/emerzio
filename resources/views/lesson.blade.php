@extends("master")
<div class="header_cover">
<div class="topmenu">
  <div class="container">
    <div class="row"  id="page-top">
      <div class="col-md-4 col-sm-6 address">
            <i class="fa fa-map-marker"></i> Ejipura, Bangalore-560045, INDIA
      </div>
      <div class="col-md-4 email text-center">
            <i class="fa fa-envelope"></i> Email: info@school.com
      </div>
      <div class="col-md-4 col-sm-6 phone">
            <i class="fa fa-mobile"></i> (91) 123 4567 890
      </div>
    </div>
  </div>
</div>
<header class="header-categories">
              <div class="container-fluid">
                  <div class="row">
                      <div class="col-md-12 mx-auto">
                          <div class="header-cont wow swing" data-wow-delay="0.3s">
                              <img src="img/logo.png" alt="Logo" class="img-fluid">
                              <h1>News &amp; Events</h1>
                          </div>
                      </div>
                  </div>
              </div>
          </header>
        </div>
@section("content")
<div class="levels_test row">
  @foreach($lessons as $lesson)
  <div class="offset-1 col-2  offset-1 align-items-center justify-content-center card bg-light text-dark shadow levels">
    <div class="row card-body">
      <h3 class="text text-center">
        {{$lesson->name}}
      </h3>
    </div>
    <div class="row card-body">
      @if($lesson->lessonId==$user->lesson_id+1)
      <a href="{{ url('level1', $lesson->lessonId) }}" class="btn btn-info question_next_btn center-block">
        Start
      </a>
      @endif @if($lesson->passed=="notPassed"&&$lesson->lessonId!=$user->lesson_id+1)
      <a  class="btn btn-secondary question_next_btn center-block">
        Start
      </a>
       @elseif($lesson->passed!="notPassed")
         <a href="{{ url('level1', $lesson->lessonId) }}" class="btn btn-dark question_next_btn center-block">
           Passed
         </a>
           @endif
         </div>
       </div>
       <div class="offset-1"></div>@endforeach</div>
<script>
var parts = window.location.pathname.split('/');
var level_id = parts.pop() || parts.pop();

</script>
@stop
