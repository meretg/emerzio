@extends('layouts.auth')


<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="{{ url('/') }}"><img src="{{ asset('/images/icon(512x512).png')}}" style="width:50px; height:50px;"><b>Emerzio</b></a>
        </div><!-- /.login-logo -->


        @section('htmlheader_title')
            Log in
        @endsection
        @if(Session::has('flash_message'))
           <div class="alert alert-info">
               {{ Session::get('flash_message') }}
           </div>
        @endif
        @if($errors->any())
           <div class="alert alert-danger">
               @foreach($errors->all() as $error)
                   <p>{{ $error }}</p>
               @endforeach
           </div>
        @endif
        @section('content')
    <div class="login-box-body">

    <form action="{{ action('adminController@admin_login') }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group has-feedback">
            <input type="email" class="form-control" placeholder="{{ trans('adminlte_lang::message.email') }}" name="email"/>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="{{ trans('adminlte_lang::message.password') }}" name="password"/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
            <!-- /.col -->
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">{{ trans('adminlte_lang::message.buttonsign') }}</button>
            </div><!-- /.col -->
        </div>
    </form>





</div><!-- /.login-box-body -->

</div><!-- /.login-box -->

    @include('layouts.partials.scripts_auth')

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
</body>

@endsection
