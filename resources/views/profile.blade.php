@extends("master")
@section("content")
<div class="row align-items-center justify-content-center">
  <div class="card shadow profile_block">
  <div class="card-body">
<form class="form-horizontal" id="profile_form" enctype="multipart/form-data" action="{{ action('UserController@updateImage') }}" method = "post">

            <div class="form-group">
              <div class="row align-items-center justify-content-center">
                <img id="profile_pic"  src="images/{{ $user->image }}" class="profilepic align-items-center justify-content-center" alt="your image">
              </div>
              <div class="row align-items-center justify-content-center">
                <label class="col-xs-12 text-center change_photo">
                  <i class="fa fa-camera" aria-hidden="true"></i>
  change photo <input class="center-block" type="file" id="image" name="image" onchange="submit_form();" style="display:none;"/>
                </label>
                <span id="invalid_image" class="text-center"></span>

                <input type="hidden" id="id" value="{{$user->id}}" name ="id"/>
              </div>


            </div>
            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
            <!-- <div class="form-group">
              <div class="row center-block">
                <button type="submit"  class="btn yellow profile_save center-block"  id="add_save" >save</button>
              </div>
            </div> -->
        </form>



<div class="row align-items-center justify-content-center">
<p>username: {{$user->username}}</p>
</div>
<div class="row align-items-center justify-content-center">
  <p>Email: {{$user->email}}</p>
  </div>
  <div class="row align-items-center justify-content-center">
    <p>password: <a data-toggle="modal" data-target="#editpass" href="#">Change password</a></p>
    </div>

  <div class="row align-items-center justify-content-center">
    <button class="btn btn-danger edit_button" data-toggle="modal" data-target="#editModal">
      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

 Edit profile</button>
  </div>
  </div>
</div>
</div>
  <div class="modal fade" id="editModal" role="dialog" data-backdrop="static">

    <div class="modal-dialog formstyle">

      <!-- Modal content-->
      <div class="modal-content  formstyle" >
        <div class="modal-header">
          <h4>Edit Profile</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>


        </div>
        <div class="modal-body">

          <div class="register-pop">

          <div id="registerBox">
            <form id="registerForm" action="{{ action('UserController@updateUser') }}" method = "post">
                <fieldset id="registerbody">
                  <fieldset>

                      <input onblur="validateUsername();" class="col-xs-12" required id="username" value="{{$user->username}}" name="username" placeholder="Username..." type="text">
                      <span id="invaliduser"></span>
                  </fieldset>
                  <fieldset>

                      <input onblur="validateEmail();" id="email" name="email" value="{{$user->email}}" placeholder="Your email..." type="email">
                      <span id="invalidEmail"></span>
                  </fieldset>
                  <fieldset>

                    <input onblur="validatepassword();" id="password" required name="password" value="{{$user->password}}" placeholder="Your password..." type="password">
                    <span id="invalidpassword"></span>
                   </fieldset>

                   <fieldset>


                   <input type="hidden" id="id" value="{{$user->id}}" name ="id"/>
                 </fieldset>
                 <div class="row align-items-center justify-content-center">
                  <input class="btn btn-info" value="save" type="submit" >
                </div>
                  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                </fieldset>

           </form>

        </div>
        </div>
        </div>

      </div>

    </div>
  </div>
  <div class="modal fade" id="editpass" role="dialog" data-backdrop="static">

    <div class="modal-dialog formstyle">

      <!-- Modal content-->
      <div class="modal-content formstyle">
        <div class="modal-header"  >
          <h4>Change password</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>


        </div>
        <div class="modal-body">

          <div class="register-pop">

          <div id="registerBox">
            <form id="registerForm" action="{{ action('UserController@updatePassword') }}" method = "post">
                <fieldset id="registerbody">
                  <fieldset>

                      <input class="col-xs-12" required id="current_password" name="oldPassword" placeholder="Current password..." type="password">
                      <!-- <span id="invaliduser"></span> -->
                  </fieldset>
                  <fieldset>
                      <input onblur="check_pass();" id="new_password" name="newPassword"  placeholder="New password..." type="password">
                      <span id="invalidpass"></span>
                  </fieldset>
                  <fieldset>
                    <input onblur="check_confirmpass();" id="confirme_password" required name="confirme_password"  placeholder="Confirm password..." type="password">
                     <span id="invalidconfirmpass"></span>
                   </fieldset>
                   <fieldset>

                 </fieldset>
                 <div class="row align-items-center justify-content-center">
                  <input class="btn btn-info" value="save" type="submit"  onclick="return change_password();">
                </div>
                  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                </fieldset>

           </form>

        </div>
        </div>
        </div>

      </div>

    </div>
  </div>
  <script type="text/javascript" src="{{ asset('/js/profile.js')}}"></script>


@stop
