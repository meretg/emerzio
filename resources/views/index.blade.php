@extends("master")
<!-- TOPNAVBAR
					=================-->
					<div class="header_cover">
					<div class="topmenu">
						<div class="container">
							<div class="row"  id="page-top">
								<div class="col-md-4 col-sm-6 address">
											<i class="fa fa-map-marker"></i> Ejipura, Bangalore-560045
								</div>
								<div class="col-md-4 email text-center">
											<i class="fa fa-envelope"></i> Email: info@school.com
								</div>
								<div class="col-md-4 col-sm-6 phone">
											<i class="fa fa-mobile"></i> (91) 123 4567 890
								</div>
							</div>
						</div>
					</div>

					<!-- HEADER
				=================-->
				<header>
						<div class="container-fluid">
								<div class="row">
										<div class="col-md-12 mx-auto">
												<div class="header-cont wow swing" data-wow-delay="0.3s">
														<img src="img/icon(512x512).png" alt="Logo" class="img-fluid" style="width:45%; height:40%;">
														<h1>Emerzio</h1>
														<a href="#about" class="js-scroll-trigger"><i class="fa fa-angle-double-down"></i></a>
												</div>
										</div>
								</div>
						</div>
				</header>
</div>
@section("content")
	<section id="about" class="about">
			<div class="container">
					<div class="row">
							<div class="col-md-6">
									<div class="about-cont wow fadeInUp" data-wow-delay="0.3s">
											<h1>About our School</h1>
											<div class="heading-bor"></div>
											<p>Our School top first in india because there are so many students at a school, it can be hard to motivate, organize and control the results of such a large school fundraiser. School management system is web based and provides support to the various modules like front desk management, student information management, payroll, account management, Inventory management and library management. It empowers the school authority to control and monitor all the task of school in an effective way.</p>
											<button class="btn btn-general btn-white">Read More <i class="fa fa-long-arrow-right"></i></button>
									</div>
							</div>
							<div class="col-md-6 my-auto">
									<div class="about-img wow fadeInUp" data-wow-delay="0.6s">
											<img src="img/about-bg.png" alt="" class="img-fluid">
									</div>
							</div>
					</div>
			</div>
	</section>

	@stop
