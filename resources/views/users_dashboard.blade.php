@extends("admin_master")
@section("content")
<section >
      <div class="container panel panel-default panel-body">
<hr>
<h2 class="col-xs-6 ">Users</h2>
<button class="col-xs-2 btn btn-success pull-right add_admin" data-toggle="modal" data-target="#adduserModal" onclick="levelslesons();">Add user</button>
<span class="clearfix"></span>
<hr>

 @if(Session::has('flash_message'))
    <div class="alert alert-info">
        {{ Session::get('flash_message') }}
    </div>
@endif
@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

<div class="modal fade" id="adduserModal" role="dialog">
<div class="modal-dialog" >

<!-- Modal content-->

<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Add user</h4>
  </div>
  <div class="modal-body">

     <form class="form-horizontal" enctype="multipart/form-data" action = "{{ action('adminController@addUser') }}"  method = "post">
<div class="form-group"  style="margin-left:1px;">
  <label class="control-label col-sm-3" >Username</label>
<div class="col-sm-6">
<input type="text" class="form-control" id="user_name" name = "username" onblur="validateUsername_user()">
<span id="user_nameinvalid"></span>
</div>


</div>

<div class="form-group" style="margin-left:1px;">
  <label class="control-label col-sm-3">Email </label>
<div class="col-sm-6">
<input type="text" class="form-control" id="user_email" name = "email" onblur="validateuseremail()">
<span id="user_emailinvalid"></span>
</div>

</div>
<div class="form-group"  style="margin-left:1px;">
  <label class="control-label col-sm-3">Password</label>
  <div class="col-sm-6">
    <input type="password" class="form-control" id="user_password" name = "password" onblur="validatepassword_user()">
    <span id="user_passwordinvalid"></span>
  </div>


</div>
<div class="form-group"  style="margin-left:1px;">
  <label class="control-label col-sm-3" >Select level</label>
  <div class="col-sm-7">
  <div class="btn-group">
  <select id="select_userlevel" name="level_id" onchange="display_question_inputs();">
  </select>
  </div>
</div>
</div>

<div class="form-group"  style="margin-left:1px;">
  <label class="control-label col-sm-3" >Select lesson</label>
  <div class="col-sm-7">
  <div class="btn-group">
  <select id="select_userlesson" name="lesson_id">
  </select>

  </div>
</div>
</div>

<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
<div class="form-group">
<div class=" savebutton">
<button type="submit" class="btn btn-default "  id="add_save" onsubmit="return validate();">Save</button>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
<div class="responsive">
<table class="table">
    <thead>
      <tr >

        <th>Username</th>
        <th>Email</th>
        <th>Photo</th>
        <th>Level</th>
        <th>Lesson</th>
        <th>Edit</th>
        <th>Delete</th>
      </tr>
    </thead>
    <tbody>

    @foreach($users as $user)

        <tr >
            <td>{{$user->username}} <br/></td>
            <td>{{$user->email}}</td>
            <td><img src="{{asset('/images')}}/{{$user->image}}" class="image"></td>
            <td>{{$user->level}}</td>
            <td>{{$user->lesson}}</td>

            <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#edituserModal"  onclick = "edit_user('{{$user->id}}');" id="user_Edit" >Edit</button></td>
          <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteuserModal"  onclick = "delete_user('{{$user->id}}');" id="user_delete" >Delete</button></td>
        </tr>

  @endforeach
    </tbody>
  </table>
  {!! $users->render()!!}
</div>
  <div class="modal fade" id="edituserModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit this user</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" action = "{{ action('adminController@editUser') }}" method = "get">

  <div class="form-group">
    <label class="control-label col-sm-2" >username</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="username" name="username">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" >Email</label>
    <div class="col-sm-10">
      <input type="email" class="form-control" id="email" name="email">

    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-sm-3" >Select level</label>
    <div class="col-sm-7">
    <div class="btn-group">
    <select id="edit_userlevel" name="level_id" onchange="display_question_inputs();">
    </select>
    </div>
  </div>
  </div>

  <div class="form-group">
    <label class="control-label col-sm-3" >Select lesson</label>
    <div class="col-sm-7">
    <div class="btn-group">
    <select id="edit_userlesson" name="lesson_id">
    </select>

    </div>
  </div>
  </div>

    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="id" id="edituser_id">
  <div class="form-group">
    <div class=" col-sm-12">
      <button  id="save_update"  type="submit" onclick="this.form.submit()"  class="btn btn-default pull-right"data-dismiss="modal" >Save</button>
    </div>
  </div>
        </form>
      </div>

    </div>
  </div>

  </div>
  <div class="modal fade" id="deleteuserModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Are you sure ?</h4>

        </div>
        <div class="modal-body">
         <strong style="color:red;">
             You will delete all information about this user
         </strong>
        </div>
        <div class="modal-footer">

          <form class="form-horizontal" action = "{{ action('adminController@deleteUser') }}" method = "post">
            <input type="hidden" name="id" id="user_id">
            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="this.form.submit()">Yes</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
          </form>


        </div>
      </div>

    </div>
  </div>

</section>
<script>
var levels=[];
var lessons=[];
window.onload =function levelslesons()
{
  $.ajax({
    url: "{{ URL::to('viewLevelsLessons') }}",
    type: "post",
    dataType: 'json',
    data: {"_token":$('#_token').val()},
    success: function(response)
    {

        levels=response.levels;
        lessons=response.lessons;

        $('#select_userlevel').html("");
        $('#edit_userlevel').html("");
        $('#select_userlevel').append($("<option selected disabled>Levels</option>"));
        $('#edit_userlevel').append($("<option selected disabled>Levels</option>"));
        for(var i=0;i<levels.length;i++)
        {
          $('#select_userlevel').append($("<option></option>")
                            .attr("value",levels[i].id)
                            .text(levels[i].name));
          $('#edit_userlevel').append($("<option></option>")
                                .attr("value",levels[i].id)
                                .text(levels[i].name));

                        }


       },
    error: function () {

        alert("error");

    }
    });
}
function edit_user(id)
{

      $("#edituser_id").val(id);
      $.ajax({
        url: "{{ URL::to('User') }}",
        type: "get",
        dataType: 'json',
        data: {"_token":$('#_token').val(),"id":id},
        success: function(response)
        {
           $("#username").val(response.username),
           $("#email").val(response.email)
           },


        error: function () {

            alert("error");

        }
        });
}
function delete_user (id)
{
  $("#user_id").val(id);
}
</script>
@stop
