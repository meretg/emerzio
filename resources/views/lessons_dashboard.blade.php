@extends("admin_master")
@section("content")
<section >
      <div class="container panel panel-default panel-body">
<hr>
<h2 class="col-xs-6 ">Lessons</h2>
<button class="col-xs-2 btn btn-success pull-right add_admin" data-toggle="modal" data-target="#addlessonModal">New lesson</button>
<span class="clearfix"></span>
<hr>

 @if(Session::has('flash_message'))
    <div class="alert alert-info">
        {{ Session::get('flash_message') }}
    </div>
@endif
@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif
<div class="modal fade" id="addlessonModal" role="dialog">
<div class="modal-dialog" >

<!-- Modal content-->

<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Add lesson</h4>
  </div>
  <div class="modal-body">

<div class="form-horizontal" enctype="multipart/form-data">
<div class="form-group "  style="margin-left:1px;">
  <label class="control-label col-sm-3" >Select level</label>
  <div class="col-sm-9">
  <div class="btn-group">
  <select id="select_level"  onchange="displaylessoninput();">
    <option selected disabled>Levels</option>
    @foreach($levels as $level)
    <option value="{{$level->id}}" name="{{$level->id}}">{{$level->name}}</option>

    @endforeach
  </select>

  </div>
</div>
</div>
<div class="form-group" id="lesson_input" style="display:none;" >
  <label class="control-label col-sm-3" >Lesson name</label>
<div class="col-sm-6" >
<input type="text" class="form-control" id="lesson_name" name = "name" required >
</div>
</div>
<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
<div class="form-group ">
<div class=" savebutton">
<button  class="btn btn-default" onclick="addlesson();"  id="add_save" >Save</button>
</div>
</div>
</div>
</div>
</div>

</div>
</div>
<div class="form-group">
  <label class="control-label col-sm-3" >Select level</label>
  <div class="col-sm-9">
  <div class="btn-group">
  <select id="show_lessons"  onchange="showlessons();">
    <option selected disabled>Levels</option>
    @foreach($levels as $level)
    <option value="{{$level->id}}" name="{{$level->id}}">{{$level->name}}</option>

    @endforeach
  </select>

  </div>
</div>
</div><br/><br/><br/>
<div id="lessons">
</div>
<div class="modal fade" id="editlessonModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit this lesson</h4>
      </div>
      <div class="modal-body">
          <form class="form-horizontal" action = "{{ action('adminController@editLesson') }}" method = "post">

<div class="form-group">
  <label class="control-label col-sm-2" >Lesson name</label>
  <div class="col-sm-10">
    <input type="text" class="form-control" id="lessonname" name="name">
  </div>
</div>

  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="id" id="editlesson_id">
<div class="form-group">
  <div class=" col-sm-12">
    <button  id="save_update"  type="submit" onclick="this.form.submit()"  class="btn btn-default pull-right"data-dismiss="modal" >Save</button>
  </div>
</div>
      </form>
    </div>

  </div>
</div>

</div>

<div class="modal fade" id="deletelessonModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Are you sure ?</h4>

      </div>
      <div class="modal-body">
       <strong style="color:red;">
           You will delete all information about this lesson
       </strong>
      </div>
      <div class="modal-footer">

        <form class="form-horizontal" action = "{{ action('adminController@deleteLesson') }}" method = "post">
          <input type="hidden" name="id" id="lesson_id">
          <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
          <button type="button" class="btn btn-default" data-dismiss="modal" onclick="this.form.submit()">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </form>


      </div>
    </div>

  </div>
</div>
</section>
<script>
function addlesson()
{
  $.ajax({
    url: "{{ URL::to('addLesson') }}",
    type: "post",
    dataType: 'json',
    data: {"_token":$('#_token').val(),"name":$("#lesson_name").val(),"level_id":$('#select_level :selected').val()},
    success: function(response)
    {
      $('#addlessonModal').modal('toggle');
       },


    error: function () {

        alert("error");

    }
    });
}
function showlessons()
{var html="";
  $.ajax({
    url: "{{ URL::to('viewLevel') }}",
    type: "post",
    dataType: 'json',
    data: {"_token":$('#_token').val(),"level_id":$('#show_lessons :selected').val()},
    success: function(response)
    {
          html+='<div class="responsive"><table class="table"><thead><tr><th>Lesson name</th><th>Edit</th><th>Delete</th></tr></thead><tbody>';
          for(var i=0;i<response.length;i++)
          {
            html+='<tr><td>'+response[i].name+'<br/></td><td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editlessonModal" onclick="editlesson('+JSON.stringify(response[i].id)+')"  id="lesson_Edit" >Edit</button></td><td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deletelessonModal" onclick="deletelesson('+JSON.stringify(response[i].id)+')" id="lesson_delete" >Delete</button></td></tr>';
        }
        html+='</tbody></table></div>';
        $("#lessons").html(html);
       },


    error: function () {

        alert("error");

    }
    });
}
function editlesson(id)
{$("#editlesson_id").val(id);
  $.ajax({
    url: "{{ URL::to('Lesson') }}",
    type: "get",
    dataType: 'json',
    data: {"_token":$('#_token').val(),"id":id},
    success: function(response)
    {

      $('#lessonname').val(response);

       },


    error: function () {

        alert("error");

    }
    });
}
function deletelesson(id)
{
  $("#lesson_id").val(id);
  showlessons();

}
function displaylessoninput()
{
  document.getElementById("lesson_input").style.display = "block";
}
</script>
@stop
