@extends("admin_master")
@section("content")
<section >
      <div class="container panel panel-default panel-body">
<hr>
<h2 class="col-xs-12 col-md-8 ">Questions</h2>
<button class="col-md-offset-2 col-md-2 col-xs-12  btn btn-success add_admin" data-toggle="modal" data-target="#addquestionModal" onclick="levelslesons();">Add question</button>
<span class="clearfix"></span>
<hr>

 @if(Session::has('flash_message'))
    <div class="alert alert-info">
        {{ Session::get('flash_message') }}
    </div>
@endif
@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif
<div class="modal fade" id="addquestionModal" role="dialog">
<div class="modal-dialog" >

<!-- Modal content-->

<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Add question</h4>
  </div>
  <div class="modal-body">

<form class="form-horizontal" enctype="multipart/form-data" action="{{ action('adminController@addQuestion') }}" method="post">
<div class="form-group row">
  <label class="control-label col-xs-2 text-center" >Select level</label>
  <div class="col-xs-9">
  <div class="btn-group center-block">
  <select id="select_level" name="level_id" onchange="display_question_inputs();">


  </select>

  </div>
</div>
</div>
<div  id="lesson_input" style="display:none;" >
  <div class="form-group">
    <label class="control-label col-xs-3 text-center" >Select lesson</label>
    <div class="col-xs-7">
    <div class="btn-group center-block">
    <select id="select_lesson" name="lesson_id" onchange="display_question();">
    </select>

    </div>
  </div>
  </div>
  </div>
  <div  id="question_inputs" style="display:none;" >
  <div class="form-group row">
    <label class="control-label col-sm-4" >Select question type</label>
    <div class="col-sm-7">
    <div class="btn-group">
    <select id="select_type" name="qType_id">


    </select>

    </div>
  </div>
  </div>
  <div class="form-group row" >
  <label class="control-label col-sm-3" >Word</label>
<div class="col-sm-9">
    <div class="btn-group">
<input type="text" class="form-control" id="word" name = "contentText" required >
</div>
</div>
</div>
  <div class="form-group row" >
<label class="control-label col-sm-3" >Image</label>
<div class="col-sm-5">
   <input class="center-block" type="file" id="image" name="image" onchange="checkextension();"/>
   <span id="invalid_image"></span>
</div>
</div>
  <div class="form-group row">
<label class="control-label col-sm-3" >Audio</label>
<div class="col-sm-5">
<input class="center-block" type="file" id="audio" name="audio" onchange="checkextension();" />
<span id="invalid_audio"></span>
</div>
</div>
</div>

<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
<div class="form-group">
<div class=" savebutton">
<button type="submit" class="btn btn-default "   id="add_save" >Save</button>
</div>
</div>
</form>
</div>
</div>

</div>
</div>

<div class="form-group row"   style="margin-left:1px;" >
  <label class="control-label col-sm-2" >Select level</label>
  <div class="col-sm-3">
  <div class="btn-group">
  <select id="select_level_board" name="level_id" onchange="display_question_inputs();">
  </select>
  </div>
</div>
</div>
<div class="form-group row" id="lessons" style="display:none;">
  <label class="control-label col-sm-2 " >Select lesson</label>
  <div class="col-sm-3">
  <div class="btn-group">
  <select id="select_lesson_board" name="lesson_id" onchange="get_questions();">
  </select>

  </div>
</div>
</div>
<div id="content">

</div>
<div class="modal fade" id="deletequestionModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Are you sure ?</h4>

      </div>
      <div class="modal-body">
       <strong style="color:red;">
           You will delete all information about this question
       </strong>
      </div>
      <div class="modal-footer">

        <form class="form-horizontal" action = "{{ action('adminController@deleteQuestion') }}" method = "post">
          <input type="hidden" name="id" id="delete_question_id">
          <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
          <button type="button" class="btn btn-default" data-dismiss="modal" onclick="this.form.submit()">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </form>


      </div>
    </div>

  </div>
</div>

</section>
<script>
window.onload =function levelslesons()
{
  $.ajax({
    url: "{{ URL::to('viewLevelsLessons') }}",
    type: "post",
    dataType: 'json',
    data: {"_token":$('#_token').val()},
    success: function(response)
    {
        levels=response.levels;
        lessons=response.lessons;
        question_types=response.types;
        $('#select_level').html("");
        $('#select_type').html("");
        $("#select_level_board").html("");
        $('#select_level').append($("<option selected disabled>Levels</option>"));
        $('#select_level_board').append($("<option selected disabled>Levels</option>"));
        for(var i=0;i<levels.length;i++)
        {
          $('#select_level').append($("<option></option>")
                            .attr("value",levels[i].id)
                            .text(levels[i].name));
          $('#select_level_board').append($("<option></option>")
                            .attr("value",levels[i].id)
                            .text(levels[i].name));
                          }

          $('#select_type').append($("<option selected disabled>question types</option>"));
            for(var i=0;i<question_types.length;i++)
            {
              $('#select_type').append($("<option></option>")
                               .attr("value",question_types[i].id)
                               .text(question_types[i].name));
          }
       },
    error: function () {

        alert("error");

    }
    });
};

function get_questions()
{var html=""
    $("#content").html("");
  $.ajax({
    url: "{{ URL::to('viewQuestions') }}",
    type: "post",
    dataType: 'json',
    data: {"_token":$('#_token').val(),"level_id":$('#select_level_board :selected').val(),"lesson_id":$('#select_lesson_board :selected').val()},
    success: function(response)

    {

      html+='<div class="responsive"><table class="table"><thead><tr><th>Text</th><th>Image</th><th>Audio</th><th>Type</th><th>Level</th><th>Lesson</th><th>Delete</th></tr></thead><tbody>';
      for(var i=0;i<response.length;i++)
      {
        html+='<tr><td>'+response[i].questions.contentText+'</td><td><img class="imge" src="{{ asset("/images")}}/'+response[i].questions.contentImg+'"></td><td><audio controls ><source src="{{ asset("../storage/app")}}/'+response[i].questions.contentAudio+'" type="audio/mpeg">Your browser does not support the audio element.</audio></td><td>'+response[i].type+'</td><td>'+response[i].level+'</td><td>'+response[i].lesson+'</td><td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deletequestionModal" onclick="deletequestion('+response[i].questions.id+');">Delete</button></td></tr>';
    }

    html+='</tbody></table></div>';
  
    $("#content").html(html);
    },
    error: function () {

        alert("error");

    }
    });

}

function deletequestion(id)
{
  $("#delete_question_id").val(id);
}
</script>
@stop
