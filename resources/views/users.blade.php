@extends("admin_master")

@section("content")

  <button class="btn btn-success adduser" id="adduser" data-toggle="modal" data-target="#mymodel">Add User</button>

  <div id="mymodel" class="modal fade" role="dialog">
  <div class="modal-dialog">
  <div class="modal-content" id="popupadd">
  <div class="modal-header">
  <button class="close" data-dismiss="modal">&times;</button>
  <h3 class="modal-title text-center"> Add user</h3>
  </div>

  <form class="modal-body" id="form_add" role="form"  action = "{{ action('adminController@addUser') }}">

 <div class="form-group">
    <div class="row"><div class="col-lg-3">
      <label> User name <label></div>
    <div class="col-sm-9">
     <input type="text"id="user_name"class="classname" requird="requird">
	    <span class="text-center" id="username_error_message"></span>
   </div>
 </div>
 </div>

<div class="form-group">
   <div class="row"><div class="col-lg-3">
	   <label>email </label></div>
     <div class="col-sm-9">
     <input type="text" id="user_email" class="classemail"requird="requird">
	     <span class="text-center" id="email_error_message"></span>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="row"><div class="col-lg-3">
   <label>Password</label></div>
   <div class="col-lg-9">
     <input type="password" class="classpassword" id="form_password" requird></div><br>
     <div class="col-lg-12"><span id="password_error_message"></span>
   </div>
 </div>
</div>






  <div class="form-group">
     <div class="row"><div class="col-lg-3">
     <label>Level</label></div>
     <div class="col-sm-9">

     <select class="level_select">
    
     </select>

     </div>
   </div>
</div>




   <div class="form-group">
      <div class="row"><div class="col-lg-3">
     <label>Lesson</label></div>
     <div class="col-sm-9">

       <select class="lesson_select">

       </select>

   </div>
 </div>
 </div>



</form>

  <div class="modal-footer"></div>
  <div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12 center-block">

	<button class="btn btn-primary btn1 text-center" id="addbtn" data-dismiss="modal">Add</button>
	<button class="btn btn-danger btn2 text-center" id="btn2" data-dismiss="modal">Cancel</button>
	</div>
	</div>
  </div>

  </div>
  </div>







	<div class="container" style="margin-top:35px">
	<table border="1px" id="tabledit" class="table table-bordered table-striped">
	<thead>
	   <tr>

	    <th class="text-center">User Name</th>
		  <th class="text-center">Email</th>
		  <th class="text-center">Level</th>
      <th class="text-center">lesson</th>
		  <th class="text-center">Photo</th>
		  <th class="text-center">Edit</th>
		  <th class="text-center">Delete</th>
	   </tr>
	   </thead>
	   <tbody id="txt">
	    <tr>

    @foreach($users as $user)

            <td class="text-center">{{$user->username}} <br/></td>
            <td class="text-center">{{$user->email}}</td>
            <td class="text-center">{{$user->level_id}}</td>
            <td class="text-center">{{$user->lesson_id}}</td>
            <td class="text-center"><img src="{{asset('/images')}}/{{$user->image}}"></td>



    <td><button class="btn btn-primary btn3 center-block" id="edit_userrr" data-toggle="modal" data-target="#edit_user" onclick="edit_user('{{$user->id}}');">edit </button> </td>
		<td><button class="btn btn-danger btn4 center-block" id="delete_user" data-toggle="modal" data-target="#delete_modal"  onclick = "delete_user('{{$user->id}}');">delete</button></td>
  @endforeach
		</tr>


	  </tbody>
    </table>
  </div>

    <!--popup edit-->

     <div id="edit_user" class="modal fade" role="dialog">
     <div class="modal-content">
      <div class="modal-header">
      <button class="close" data-dismiss="modal">&times;</button>
      <h3 class="modal-title text-center"> edit user</h3>
      </div>
    <div class="modal-body">

        <form class="modal-body" id="form_add" action = "{{ action('adminController@editUser') }}" method = "post" role="form">

        <div class="form-group">
          <div class="row"><div class="col-lg-3">
            <label> User name <label></div>
          <div class="col-sm-9">
           <input type="text" id="user2_name" class="classname" requird="requird">
            <span class="text-center" id="username2_error_message"></span>
         </div>
        </div>
        </div>

        <div class="form-group">
         <div class="row"><div class="col-lg-3">
           <label>email </label></div>
           <div class="col-sm-9">
           <input type="text" id="user2_email" class="classemail"requird="requird">
             <span class="text-center" id="email2_error_message"></span>
          </div>
        </div>
        </div>

        <div class="form-group">
        <div class="row"><div class="col-lg-3">
         <label>Password</label></div>
         <div class="col-lg-9">
           <input type="password" class="classpassword" id="form_password2" requird></div><br>
           <div class="col-lg-12"><span id="password2_error_message"></span>
         </div>
        </div>
        </div>

        <div class="form-group">
           <div class="row"><div class="col-lg-3">
           <label>Level</label></div>
           <div class="col-sm-9">
             <select class="level_select">


             </select>

           </div>
         </div>
         </div>

         <div class="form-group">
            <div class="row"><div class="col-lg-3">
           <label>Lesson</label></div>
           <div class="col-sm-9">
             <select class="lesson_select">

             </select>

         </div>
        </div>
        </div>



    </form>

      <div class="modal-footer"></div>
      <div class="row">
    	<div class="col-lg-12 col-sm-12 col-xs-12 center-block">
    	<button  id="save_update"  type="submit" onclick="this.form.submit()"  class="btn btn-success"data-dismiss="modal" >Save</button>
    	<button class="btn btn-danger btn2 text-center" id="btn2" data-dismiss="modal">Cancel</button>
    </div>
  </div>
</div>
</div>
</div>

      <!--popup delete-->
      <div class="modal fade" id="delete_modal" role="dialog">
        <div class="modal-dialog">

          <div class="modal-content" id="popupdelete">
            <div class="modal-header">
              <button type="button"  class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Are you sure ?</h4>

            </div>
            <div class="modal-body">
             <strong style="color:red;">
                 You will delete all information about this user
             </strong>
            </div>
            <div class="modal-footer">

              <form class="form-horizontal" action = "{{ action('adminController@deleteUser') }}" method = "post">
                <input type="hidden" name="id" id="user_id">
                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="this.form.submit()">Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
              </form>


            </div>
          </div>

        </div>
      </div>

  <script>
      var userId;
      var edituser_id;

      function edit_user(id)
     {
            edituser_id=id;
            $("#edituser_id").val(id);
            $.ajax({
              url: "{{ URL::to('viewUser') }}",
              type: "GET",
              dataType: 'json',
              data: {"_token":$('#_token').val(),"id":id},
              success: function(response)
              {
                 $("#username").val(response.username),
                 $("#email").val(response.email),
                 $("#password").val(response.password)
                 },


              error: function () {

                  alert("error");

              }
              });
  }
  function delete_user (id)
  {
    userId = id;

    $("#user_id").val(userId);
  }


  <!--
  $("#adduser").on("click",function()
  {
   var levels=[];
   var lessons=[];
 $.ajax({
   url: "{{ URL::to('viewLevelsLessons') }}",
   type: "GET",
   dataType: 'json',
   data: {"_token":$('#_token').val(),"id":id},

   success: function(response)
   {
     levels=response.levels;
     lessons=response.lessons;

     $(".level_select").on("change",function(){

        for(i=0;i<lessons.length;i++)
         {
           if($(this).find(":selected").val()==lessons[i].level_id)
           {
             $(".lesson_selected").attr("value",lessons[i].id)

             $(".lesson_selected").text("<option> lessons[i].name </option>")


          //     $(".lesson_select").find(":selected").val()=lessons[i].name;
         }
       }

      });

   }
});
});

-->
    </script>
@stop
