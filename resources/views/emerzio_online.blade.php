@extends("master")

<div class="header_cover">
<div class="topmenu">
  <div class="container">
    <div class="row"  id="page-top">
      <div class="col-md-4 col-sm-6 address">
            <i class="fa fa-map-marker"></i> Ejipura, Bangalore-560045, INDIA
      </div>
      <div class="col-md-4 email text-center">
            <i class="fa fa-envelope"></i> Email: info@school.com
      </div>
      <div class="col-md-4 col-sm-6 phone">
            <i class="fa fa-mobile"></i> (91) 123 4567 890
      </div>
    </div>
  </div>
</div>
<header class="header-categories">
              <div class="container-fluid">
                  <div class="row">
                      <div class="col-md-12 mx-auto">
                          <div class="header-cont wow swing" data-wow-delay="0.3s">
                              <img src="img/icon(512x512).png" alt="Logo" class="img-fluid" style="width:45%; height:40%;">
                              <h1>Emerzio Online</h1>
                          </div>
                      </div>
                  </div>
              </div>
          </header>
        </div>
@section("content")

<!-- <a href="#" onclick="get_questions();"  id="question"  class="btn yellow question_next_btn center-block">View question</a> -->
<div id="content">
@if($user->level_id==Null)
<div class="levels_test">
  <div class="row align-items-center justify-content-center title_levels_test">
    <h1 class="text-center text">Let's get started!</h1>
  </div>
  <div class="row align-items-center justify-content-center">
    <div class="col-sm-4 card bg-light text-dark shadow">
    <div class="row align-items-center justify-content-center card-body ">
        <div class="col-xs-12 col-sm-4 ">
      <img src="img/boy.png" style="width:50%; border-radius:50%;" class="align-items-center justify-content-center">
    </div>
    <div class="col-xs-12 col-lg-6">
      <div class="row">
        <h5>I'm a new to Arabic!</h5>
      </div>
      <div class="row">
        <a href="#" onclick="get_levels();" id="question"  class="btn align-items-center justify-content-center btn-outline-info">Start at Level 1</a>
      </div>
    </div>
    </div>
  </div>
    <div class="col-sm-2 ">
      <h2 class="text-center">OR</h2>
    </div>
    <div class="col-sm-4 card bg-light text-dark shadow">
      <div class="row align-items-center justify-content-center card-body">
    <div class="col-xs-12 col-sm-4">
      <img src="img/rocket.png" class="center-block" style="width:50%;">
    </div>

    <div class="col-xs-12 col-lg-6">
      <div class="row">
        <h5>Find my level!</h5>
      </div>
      <a href="#" class="btn btn-outline-info">Take a quick placement test</a>
    </div>
  </div>
    </div>
  </div>
</div>
@else

<div class="levels_test row">
  @foreach($levels as $level)
  <div class="offset-1 col-2  offset-1 align-items-center justify-content-center card bg-light text-dark shadow levels">
    <div class="row card-body">
      <h3 class="text text-center">
        {{$level->name}}
      </h3>
    </div>
    <div class="row card-body">
      @if($level->levelId==$user->level_id+1)
      <a href="{{ url("lesson", $level->levelId) }}" class="btn btn-info question_next_btn center-block">
        Start
      </a>
      @endif @if($level->passed=="notPassed"&&$level->levelId!=$user->level_id+1)
      <a  class="btn btn-secondary question_next_btn center-block">
        Start
      </a>
       @elseif($level->passed!="notPassed")
         <a href="{{ url("lesson", $level->levelId) }}" class="btn btn-dark question_next_btn center-block">
           Passed
         </a>
           @endif
         </div>
       </div>
       <div class="offset-1"></div>
@endforeach
</div>

   @endif
</div>


<script>
  var html="";
function get_levels()
{
  html+='<div class="levels_test row">@foreach($levels as $level)<div class="offset-1 col-2  offset-1 align-items-center justify-content-center card bg-light text-dark shadow levels"><div class="row card-body"><h3 class="text text-center">{{$level->name}}</h3></div><div class="row card-body">@if($level->levelId==$user->level_id+1)<a href="{{ url("lesson", $level->levelId) }}" class="btn btn-info question_next_btn center-block">Start</a>@endif @if($level->passed=="notPassed"&&$level->levelId!=$user->level_id+1)<a  class="btn btn-secondary question_next_btn center-block">Start</a> @elseif($level->passed!="notPassed")  <a href="{{ url("lesson", $level->levelId) }}" class="btn btn-dark question_next_btn center-block">Passed</a>  @endif</div></div><div class="offset-1"></div>@endforeach</div>';
$("#content").html(html);
}

</script>
@stop
