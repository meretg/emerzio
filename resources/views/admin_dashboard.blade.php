@extends("admin_master")
@section("content")
<section >
      <div class="container panel panel-default panel-body">
<hr>
<h2 class="col-xs-6 ">Admins</h2>
<button class="col-xs-2 btn btn-success pull-right add_admin" data-toggle="modal" data-target="#addadminModal">Add admin</button>
<span class="clearfix"></span>
<hr>

 @if(Session::has('flash_message'))
    <div class="alert alert-info">
        {{ Session::get('flash_message') }}
    </div>
@endif
@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

<div class="responsive">
<table class="table">
    <thead>
      <tr >

        <th>Username</th>
        <th>Email</th>
        <th>Edit</th>
        <th>Delete</th>
      </tr>
    </thead>
    <tbody>
      <tr >

    @foreach($admins as $k => $admin)
        @if ($admin->IsAdmin)
        <td>{{$admin->username}}</td>
        <td>{{$admin->email}}</td>
        <tr >

        @else
            <td>{{$admin->username}} <br/></td>
            <td>{{$admin->email}}</td>
            <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editadminModal"  onclick = "edit_admin('{{$admin->id}}');" id="admin_Edit" >Edit</button></td>
          <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteadminModal"  onclick = "delete_admin('{{$admin->id}}');" id="admin_delete" >Delete</button></td>
        </tr>

       @endif
  @endforeach
    </tbody>
  </table>
  {!!$admins->render()!!}
</div>
  <div class="modal fade" id="addadminModal" role="dialog">
<div class="modal-dialog" >

  <!-- Modal content-->

  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Add admin</h4>
    </div>
    <div class="modal-body">

       <form class="form-horizontal" enctype="multipart/form-data" action = "{{ action('adminController@addAdmin') }}"  method = "post">
<div class="form-group" style="margin-left:1px;">
    <label class="control-label col-sm-3 "   >Username</label>
<div class="col-sm-6">
  <input type="text" class="form-control" id="admin_name" name = "username" onblur="validateUsername()">
  <span id="admin_nameinvalid"></span>
</div>


</div>

<div class="form-group" style="margin-left:1px;">
    <label class="control-label col-sm-3">Email </label>
<div class="col-sm-6">
  <input type="text" class="form-control" id="admin_email" name = "email" onblur="validateadminemail()">
  <span id="admin_emailinvalid"></span>
</div>

</div>
  <div class="form-group" style="margin-left:1px; "  >
    <label class="control-label col-sm-3">Password</label>
    <div class="col-sm-6">
      <input type="password" class="form-control" id="admin_password" name = "password" onblur="validatepassword()">
      <span id="admin_passwordinvalid"></span>
    </div>

</div>
<div class="form-group" style="margin-left:1px;">
  <label class="control-label col-sm-3">Confirm password</label>
  <div class="col-sm-6">
    <input type="password" class="form-control" id="admin_password2" name = "password2" onblur="validateconfirmpass()">
    <span id="admin_repasswordinvalid"></span>
  </div>

</div>
<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
<div class="form-group">
<div class=" savebutton">
  <button type="submit" class="btn btn-default"  id="add_save" onsubmit="return validate();">Save</button>
</div>
</div>
</form>
</div>
</div>
</div>
</div>

<div class="modal fade" id="editadminModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit this admin</h4>
      </div>
      <div class="modal-body">
          <form class="form-horizontal" action = "{{ action('adminController@editAdmin') }}" method = "post">

<div class="form-group">
  <label class="control-label col-sm-2" >username</label>
  <div class="col-sm-10">
    <input type="text" class="form-control" id="username" name="username">
  </div>
</div>
<div class="form-group">
  <label class="control-label col-sm-2" >Email</label>
  <div class="col-sm-10">
    <input type="email" class="form-control" id="email" name="email">

  </div>
</div>

  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="id" id="editadmin_id">
<div class="form-group">
  <div class=" col-sm-12 savebutton">
    <button  id="save_update"  type="submit" onclick="this.form.submit()"  class="btn btn-default pull-right"data-dismiss="modal" >Save</button>
  </div>
</div>
      </form>
    </div>

  </div>
</div>

</div>

<div class="modal fade" id="deleteadminModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Are you sure ?</h4>

      </div>
      <div class="modal-body">
       <strong style="color:red;">
           You will delete all information about this admin
       </strong>
      </div>
      <div class="modal-footer">

        <form class="form-horizontal" action = "{{ action('adminController@deleteAdmin') }}" method = "post">
          <input type="hidden" name="id" id="admin_id">
          <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
          <button type="button" class="btn btn-default" data-dismiss="modal" onclick="this.form.submit()">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </form>


      </div>
    </div>

  </div>
</div>
</section>
<script>
    var adminId;
    var editadmin_id;
    function delete_admin (id)
    {
      adminId = id;

      $("#admin_id").val(adminId);
    }
    function edit_admin(id)
{
          editadmin_id=id;
          $("#editadmin_id").val(id);
          $.ajax({
            url: "{{ URL::to('viewAdmin') }}",
            type: "GET",
            dataType: 'json',
            data: {"_token":$('#_token').val(),"id":id},
            success: function(response)
            {
               $("#username").val(response.username),
               $("#email").val(response.email),
               $("#password").val(response.password)
               },


            error: function () {

                alert("error");

            }
            });
}
  </script>
@stop
