-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 21, 2018 at 05:14 PM
-- Server version: 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `emerzio`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

CREATE TABLE `levels` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `score` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`id`, `name`, `score`, `created_at`, `updated_at`) VALUES
(1, 'level1', '20.00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` enum('login','logout') COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_100000_create_password_resets_table', 1),
('2017_11_16_094019_create_admins_table', 1),
('2017_12_05_144349_create_levels_table', 1),
('2014_10_12_000000_create_users_table ', 2),
('2017_12_05_144603_create_logs_table', 3),
('2017_12_05_144845_create_question_types_table', 3),
('2017_12_05_144536_create_questions_table', 4),
('2017_12_05_144819_create_user_answers_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `qTypes`
--

CREATE TABLE `qTypes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `qTypes`
--

INSERT INTO `qTypes` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'type0', NULL, NULL),
(2, 'type1', NULL, NULL),
(3, 'type2', NULL, NULL),
(4, 'type3', NULL, NULL),
(5, 'type4', NULL, NULL),
(6, 'type5', NULL, NULL),
(7, 'type6', NULL, NULL),
(8, 'type7', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `contentText` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contentImg` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contentAudio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `level_id` int(10) UNSIGNED NOT NULL,
  `qType_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `contentText`, `contentImg`, `contentAudio`, `level_id`, `qType_id`, `created_at`, `updated_at`) VALUES
(1, 'ولد', 'boy.png', 'Hello.mp3', 1, 1, NULL, NULL),
(2, 'بنت', 'girl.png', 'Hello.mp3', 1, 1, NULL, NULL),
(3, 'ولد يأكل', 'boy_eat.png', 'Hello.mp3', 1, 1, NULL, NULL),
(4, 'بنت تشرب', 'girl_drink.jpg', 'Hello.mp3', 1, 1, NULL, NULL),
(5, 'بنت تاكل', 'girl_eat.png', 'Hello.mp3', 1, 1, NULL, NULL),
(6, 'رجل يشرب', 'man_drink.jpg', 'Hello.mp3', 1, 1, NULL, NULL),
(7, 'امرأة', 'woman.jpg', 'Hello.mp3', 1, 1, NULL, NULL),
(8, 'بنت تشرب', 'girl_drink.jpg', 'Hello.mp3', 1, 2, NULL, NULL),
(15, 'رجل يشرب', 'man_drink.jpg', 'Hello.mp3', 1, 3, NULL, NULL),
(16, 'امرأة', 'woman.jpg', 'Hello.mp3', 1, 4, NULL, NULL),
(17, 'ولد يأكل', 'boy_eat.png', 'Hello.mp3', 1, 5, NULL, NULL),
(18, 'ولد', 'boy.png', 'Hello.mp3', 1, 6, NULL, NULL),
(19, 'بنت', 'girl.png', 'Hello.mp3', 1, 7, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `userAnswers`
--

CREATE TABLE `userAnswers` (
  `id` int(10) UNSIGNED NOT NULL,
  `wrongAnswer` enum('0','1','2','3') COLLATE utf8_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `duration` double(8,2) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `question_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'profile.jpg',
  `DOB` date NOT NULL,
  `gender` enum('male','female') COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `score` int(11) NOT NULL DEFAULT '0',
  `level_id` int(10) UNSIGNED DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `username`, `password`, `image`, `DOB`, `gender`, `country`, `score`, `level_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '', '', 'ghjh', '$2y$10$TayxZgzTYeCKfMuV90sjNufv8JaHTDSsQLgxc5Eq2QRRgP1Lr6l7y', 'profile.jpg', '0000-00-00', 'male', NULL, 0, NULL, NULL, '2018-01-15 10:52:41', '2018-01-15 10:52:41'),
(4, '', 'a1@hotmail.com', 'r', '$2y$10$nSFxUiypXOEwE8BhXKpyxeGxjIdv2x3Xx81POIB13QyyOQTCNJJHe', 'profile.jpg', '0000-00-00', 'male', NULL, 0, NULL, NULL, '2018-01-16 08:16:44', '2018-01-16 08:16:44'),
(5, '', 'admin@codex.net', 'yh', '$2y$10$pZST3uWgwqR1vr9r6HXmFOf0PP0Lph/pKUXPj7tpopmS5bQCHpH5i', 'profile.jpg', '0000-00-00', 'male', NULL, 0, NULL, NULL, '2018-01-17 06:28:29', '2018-01-17 06:28:29'),
(6, '', 'a@hotmail.com', 'rehab', '$2y$10$sngD.OfhVIiA5W/Aa/Khl.fe1IvCsk7unAEmnppWoPquiB0tcBSG6', '1516547344.png', '0000-00-00', 'male', NULL, 0, NULL, 'gEXQ7oMEqiQoH75NdmxTXBaw3HXl85SatnRcCunzsf3ApwF3HcQaC0g42xa3', '2018-01-17 07:06:07', '2018-01-21 13:09:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `logs_user_id_foreign` (`user_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `qTypes`
--
ALTER TABLE `qTypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questions_level_id_foreign` (`level_id`),
  ADD KEY `questions_qtype_id_foreign` (`qType_id`);

--
-- Indexes for table `userAnswers`
--
ALTER TABLE `userAnswers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `useranswers_user_id_foreign` (`user_id`),
  ADD KEY `useranswers_question_id_foreign` (`question_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_level_id_foreign` (`level_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `levels`
--
ALTER TABLE `levels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `qTypes`
--
ALTER TABLE `qTypes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `userAnswers`
--
ALTER TABLE `userAnswers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `logs`
--
ALTER TABLE `logs`
  ADD CONSTRAINT `logs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_level_id_foreign` FOREIGN KEY (`level_id`) REFERENCES `levels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `questions_qtype_id_foreign` FOREIGN KEY (`qType_id`) REFERENCES `qTypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `userAnswers`
--
ALTER TABLE `userAnswers`
  ADD CONSTRAINT `useranswers_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `useranswers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_level_id_foreign` FOREIGN KEY (`level_id`) REFERENCES `levels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
